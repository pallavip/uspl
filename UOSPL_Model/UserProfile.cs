﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace UOSPL_Model
{
    public class UserProfile
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PAN { get; set; }
        public string Aadhar_Number { get; set; }
        public string GST_Number { get; set; }
        public string OfficeAddress { get; set; }
        public string MobileNo { get; set; }
       
        public string Password { get; set; }

        [Required]
        [Remote("CheckUserNameExist", "Account", HttpMethod = "POST", ErrorMessage = "User with this Email already exists")]
        public string UserName { get; set; }
        public int LoginID { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
