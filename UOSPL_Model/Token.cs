﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UOSPL_Model
{
   public class Token
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public string username { get; set; }
        public string success { get; set; }
        public string message { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string mobileno { get; set; }
        public string driverlicnumber { get; set; }
        public string TmpCustomerID { get; set; }

        public string emergencycontactno { get; set; }
        public string filepath { get; set; }
        //public int usertype { get; set; }




    }
}
