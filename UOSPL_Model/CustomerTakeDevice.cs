﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UOSPL_Model
{
    public partial class Customer
    {
        public string UserName { get; set; }

        public int MachineID { get; set; }

        //public int MachineTypeID { get; set; }
        
        public int MachineTypeDeviceID { get; set; }

    }
}
