//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UOSPL_Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Customer
    {
        public int? CustomerID { get; set; }
        public Nullable<int> AdminID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string CompanyName { get; set; }
        public string MobileNo { get; set; }
        public string LandlineNo { get; set; }
        public string OfficeAddress { get; set; }
        public Nullable<int> State_ID { get; set; }
        public Nullable<int> City_ID { get; set; }
        public string PAN { get; set; }
        public string Aadhar_Number { get; set; }
        public string GST_Number { get; set; }
        public string Company_Logo_Path { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> LoginID { get; set; }
    }
}
