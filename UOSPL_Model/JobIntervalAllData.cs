﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UOSPL_Model
{
   public partial class JobIntervalAllData
    {
        public int JobIntervalID { get; set; }
        public string MachineSrNo { get; set; }
        public string OperatorCode { get; set; }
        //public string HeadType { get; set; }
        //public string ControllerType { get; set; }
        //public DateTime Date { get; set; }
        //public DateTime Time { get; set; }
        public string WeldNo { get; set; }
        public string TubeThickness { get; set; }
        public string TubeDIA { get; set; }
        public string PreHeat { get; set; }
        public string PostFlow { get; set; }
        public string PreFlow { get; set; }
        public string HighPulse { get; set; }
        public string LowPulse { get; set; }
        public string BaseCurrent { get; set; }

        public string Levels { get; set; }


        public string A1 { get; set; }
        public string A2 { get; set; }
        public string A3 { get; set; }
        public string B1 { get; set; }
        public string B2 { get; set; }
        public string B3 { get; set; }
        public string C1 { get; set; }
        public string C2 { get; set; }
        public string C3 { get; set; }
        public string D1 { get; set; }
        public string D2 { get; set; }
        public string D3 { get; set; }
        public string E1 { get; set; }
        public string E2 { get; set; }
        public string E3 { get; set; }
        public string F1 { get; set; }
        public string F2 { get; set; }
        public string F3 { get; set; }
        public string G1 { get; set; }
        public string G2 { get; set; }
        public string G3 { get; set; }
        public string H1 { get; set; }
        public string H2 { get; set; }
        public string H3 { get; set; }
        public string I1 { get; set; }
        public string I2 { get; set; }
        public string I3 { get; set; }

        public string day { get; set; }
        public string month { get; set; }
        public string year { get; set; }
        public string hrs { get; set; }
        public string min { get; set; }
        public string sec { get; set; }




    }
}
