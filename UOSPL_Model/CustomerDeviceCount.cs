﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UOSPL_Model
{
   public partial class CustomerDeviceCount
    {
        public int OfflineDeviceCount { get; set; }       
        public int OnlineDeviceCount { get; set; }
        public int TotalDeviceCount { get; set; }
        public int OfflineDeviceCount48 { get; set; }

        public List<CustomerDeviceCount> ListDeviceCount { get; set; }
    }
}
