﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;

namespace UOSPL_DL
{
    public class CustomerDL
    {
        OSPL_DEMOEntities OSPLDEMOEntities = null;

        public List<uspGetCustomersDetails_Result> GetCustomerDetails(int? customerID)
        {
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetCustomersDetails(customerID: customerID).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<uspGetDashBoardDetails_Result> GetDashboardDetailsMoreInfo(int? customerID, int flag)
        {
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetDashBoardDetails(customerID: customerID,flag:flag).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //get state details
        public List<uspGetStates_Result> GetStates()
        {
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetStates().ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }


        //get cities details
        public List<uspGetCities_Result> GetCities(int? StateID)
        {
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetCities(state_ID: StateID).ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int DeleteCustomer(int? CustomerID)
        {
            try
            {
                ObjectParameter outPutParam = new ObjectParameter("outRecordID", typeof(Int32));
                int i = 0;
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                i = OSPLDEMOEntities.uspDeleteCustomer(customerID: CustomerID, outRecordID: outPutParam);
                return Convert.ToInt32(outPutParam.Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //GET MAPPED CUSTOMER DEVICE DETAILS
        public List<uspGetMappedCustomerDevice_Result> GetMappedCustomerDevices(int? CustomerID)
        {
            //string s = "a";
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetMappedCustomerDevice(CustomerID).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //delete mapping
        public int DeleteMappedCustomerDevice(int MachineID)
        {
            try
            {
                ObjectParameter outRecordID = new ObjectParameter("OutRecordID", typeof(Int32));
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                int i = OSPLDEMOEntities.DeleteMappedCustomerDevice(MachineID, outRecordID: outRecordID);
                return Convert.ToInt32(outRecordID.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int MapCustomerDevice(Customer customer)
        {
            try
            {
                ObjectParameter outRecordID = new ObjectParameter("outRecordID", typeof(Int32));
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                int i = OSPLDEMOEntities.spMappCustomerDevice(customer.MachineID, customer.CustomerID, customer.MachineTypeDeviceID);
                return Convert.ToInt32(outRecordID.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int AddUpdateCustomer(Customer obj)
        {
            try
            {
                int i = 0;
                ObjectParameter outPutParam = new ObjectParameter("outRecordID", typeof(Int32));
                //ObjectParameter TB_Mother_Access_Token = new ObjectParameter("TB_Mother_Access_Token", typeof(Int32));
                outPutParam.Value = 0;

                OSPLDEMOEntities = new OSPL_DEMOEntities();
                i = OSPLDEMOEntities.uspAddUpdateCustomer(customerID: obj.CustomerID, firstName: obj.FirstName, middleName: obj.MiddleName, lastName: obj.LastName, emailID: obj.EmailID, companyName: obj.CompanyName, mobileNo: obj.MobileNo, landlineNo: obj.LandlineNo, officeAddress: obj.OfficeAddress, state_ID: obj.State_ID, city_ID: obj.City_ID, pAN: obj.PAN, aadhar_Number: obj.Aadhar_Number, gST_Number: obj.GST_Number, company_Logo_Path: obj.Company_Logo_Path, isActive: false, createdBy: obj.CreatedBy, updatedBy: obj.UpdatedBy, outRecordID: outPutParam);
                // i = OSPLDEMOEntities.uspAddUpdateCustomer(customerID: obj.CustomerID, name: obj.Name, mobileNo: obj.MobileNo, address: obj.Address,email:obj.Email,gSTNo:obj.GSTNo, createdBy: obj.CreatedBy, updatedBy: obj.UpdatedBy, outRecordID: outPutParam);
                if (outPutParam != null)
                {
                    //////TB_ACCESS_TOKEN = Convert.ToString(TB_Mother_Access_Token.Value);
                    //////NOW PUBLISH THE IMEI NO ,USERNAME,PASSWORD TO TB USING HTTP
                    ////string BASEURL = "http://fliotex.faralenz.in:8080/api/v1/" + "/telemetry";

                    ////var request = (HttpWebRequest)WebRequest.Create(BASEURL);

                    ////// var postData = "{\"thing1\":\"hello\"}";
                    ////var postData = "{\"username\":\"amitl@faralenz.in\",\"password\":\"amit12!@\",\"imei\":\"" + obj.MachineSerialNo + "\"}";

                    ////var data = Encoding.ASCII.GetBytes(postData);


                    ////request.Method = "POST";
                    ////// request.ContentType = "application/x-www-form-urlencoded";
                    ////request.ContentType = "application/json";
                    ////request.ContentLength = data.Length;

                    ////using (var stream = request.GetRequestStream())
                    ////{
                    ////    stream.Write(data, 0, data.Length);
                    ////}

                    ////var response = (HttpWebResponse)request.GetResponse();

                    ////var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    ////var statusNumber = (int)response.StatusCode;

                    ////if (statusNumber == 200)
                    ////{
                    ////}
                    return Convert.ToInt32(outPutParam.Value);

                }
                else
                {
                    return i;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
