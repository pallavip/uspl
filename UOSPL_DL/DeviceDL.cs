﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;


namespace UOSPL_DL
{
   public class DeviceDL
    {
        OSPL_DEMOEntities OSPLDEMOEntities = null;

        public List<uspGetDevice_Result> GetDevices(int? MachineID)
        {
            Device device = new Device();
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetDevice(machineID: MachineID).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetDevicesCount1(int? MachineID)
        {
            int deviceid = 0;
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
               // x.MachineID == MachineID  when machin id present.
                deviceid = OSPLDEMOEntities.Devices.Where(x => x.IsDeleted == false && x.IsActive == true).ToList().Count();              


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return deviceid;
        }


        public CustomerDeviceCount GetDevicesCount(int? CustomerID)
        {
 
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();


                //get id one by one to DB and get data

                CustomerDeviceCount deviceCount = new CustomerDeviceCount();
                var CustomerDevicearray = new List<CustomerDeviceCount>();

                var list = new List<CustomerDeviceCount>();



                //take trip id and send to DB
                var result1 = OSPLDEMOEntities.uspDeviceCountDetails(customerID:CustomerID).ToList();

                if (result1.Count > 0)
                {
                    foreach (var b in result1)

                    {
                        deviceCount.OfflineDeviceCount =Convert.ToInt32(b.OfflineDeviceCount);
                        deviceCount.OnlineDeviceCount = Convert.ToInt32(b.OnlineDeviceCount);
                        deviceCount.TotalDeviceCount = Convert.ToInt32(b.TotalDeviceCount);
                        deviceCount.OfflineDeviceCount48 = Convert.ToInt32(b.OfflineDeviceCount48);


                        //var s = new CustomerDeviceCount();
                        //{
                        //    s.OfflineDeviceCount = b.OfflineDeviceCount;                   
                        //    s.OnlineDeviceCount = b.OnlineDeviceCount;
                        //    s.TotalDeviceCount = b.TotalDeviceCount;
                        //    s.OfflineDeviceCount48 = b.OfflineDeviceCount48;


                        //};


                    }
                    //deviceCount.ListDeviceCount = list;



                }
                else
                {

                  
                }


                return deviceCount;
            }
            catch (Exception ex)
            {

                throw;
            }
        }



        //get Machine type details
        public List<uspGetMachineType_Result> GetMachineTypes()
        {
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetMachineType().ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int DeleteDevice(int? MachineID)
        {
            try
            {
                ObjectParameter outPutParam = new ObjectParameter("outRecordID", typeof(Int32));
                int i = 0;
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                i = OSPLDEMOEntities.uspDeleteDevice(machineID: MachineID, outRecordID: outPutParam);
                return Convert.ToInt32(outPutParam.Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddUpdateDevice(Device obj)
        {
            try
            {
                string TB_ACCESS_TOKEN = "";
                int i = 0;
                ObjectParameter outPutParam = new ObjectParameter("outRecordID", typeof(Int32));
                ObjectParameter TB_Mother_Access_Token = new ObjectParameter("TB_Mother_Access_Token", typeof(Int32));
                outPutParam.Value = 0;
   
                OSPLDEMOEntities = new OSPL_DEMOEntities();

                 i = OSPLDEMOEntities.uspAddUpdateDevice
                    (
                    machineID: obj.MachineID, 
                    machineName: obj.MachineName,
                    machineSerialNo: obj.MachineSerialNo,
                    machineTypeDeviceID:obj.MachineTypeDeviceID,
                    machineDescription: obj.MachineDescription,
                    isActive: true,
                    createdBy: 1,
                    updatedBy: 1,
                    outRecordID: outPutParam,
                    tB_Mother_Access_Token: TB_Mother_Access_Token,
                    manufacturing_ID: obj.Manufacturing_ID
                    );

                if (outPutParam != null)
                {
                    TB_ACCESS_TOKEN = Convert.ToString(TB_Mother_Access_Token.Value);
                    //NOW PUBLISH THE IMEI NO ,USERNAME,PASSWORD TO TB USING HTTP
                    string BASEURL = "http://fliotex.faralenz.in:8080/api/v1/" + TB_ACCESS_TOKEN + "/telemetry";

                    var request = (HttpWebRequest)WebRequest.Create(BASEURL);

                    // var postData = "{\"thing1\":\"hello\"}";
                    var postData = "{\"username\":\"amitl@faralenz.in\",\"password\":\"amit12!@\",\"machineSerialNo\":\"" + obj.MachineSerialNo + "\"}";

                    var data = Encoding.ASCII.GetBytes(postData);

                    request.Method = "POST";
                    // request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentType = "application/json";
                    request.ContentLength = data.Length;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();

                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    var statusNumber = (int)response.StatusCode;

                    if (statusNumber == 200)
                    {
                    }
                    return Convert.ToInt32(outPutParam.Value);

                }
                else
                {
                    return i;
                }
                //if (Convert.ToInt32(outPutParam.Value) > 0)
                //{
                //   // return Convert.ToInt32(outPutParam.Value);
                //}
                //return Convert.ToInt32(outPutParam.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
