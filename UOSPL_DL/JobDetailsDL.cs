﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;


namespace UOSPL_DL
{
   public class JobDetailsDL
    {
        OSPL_DEMOEntities OSPLDEMOEntities = null;

        public List<uspGetJobDetails_Result> GetJobDetails(int? JobDetailID)
        {
            JobDetail jobDetail = new JobDetail();
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetJobDetails(jobDetailID: JobDetailID).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddUpdateJobDetail(JobDetail obj)
        {
            try
            {
                //  int i = 0;
                ObjectParameter outPutParam = new ObjectParameter("outRecordID", typeof(Int32));
                //ObjectParameter TB_Mother_Access_Token = new ObjectParameter("TB_Mother_Access_Token", typeof(Int32));
                outPutParam.Value = 0;

                OSPLDEMOEntities = new OSPL_DEMOEntities();

                int i = OSPLDEMOEntities.uspAddUpdateJobDetails
                     (
                     jobDetailID: obj.JobDetailID,
                     machineTypeID: obj.MachineTypeID,
                     machineSerialNoID: obj.MachineSerialNoID,
                     customerName:obj.CustomerName,
                     projectDoneBy:obj.ProjectDoneBy,
                     orbitalHeadType:obj.OrbitalHeadType,
                     powerSource:obj.PowerSource,
                     machineCode:obj.MachineCode,
                     isActive: true,
                     createdBy: 1,
                     updatedBy: 1,
                     outRecordID: outPutParam

                     );

                if (Convert.ToInt32(outPutParam.Value) > 0)
                {
                    // return Convert.ToInt32(outPutParam.Value);
                }
                  return Convert.ToInt32(outPutParam.Value);
                //return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteJobDetail(int? JobDetailID)
        {
            try
            {
                ObjectParameter outPutParam = new ObjectParameter("outRecordID", typeof(Int32));
                int i = 0;
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                i = OSPLDEMOEntities.uspDeleteJobDetails(jobDetailID: JobDetailID, outRecordID: outPutParam);
                return Convert.ToInt32(outPutParam.Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
