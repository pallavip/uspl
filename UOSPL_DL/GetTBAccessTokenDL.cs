﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;
using System.Data;
using System.Data.Entity.Core.Objects;

namespace UOSPL_DL
{
   public class GetTBAccessTokenDL
    {
        OSPL_DEMOEntities OSPLDEMOEntities = null;
        public Device GetThingsBoardAccessToken(Device device)
        {

            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                ObjectParameter outRecordID = new ObjectParameter("OutRecordID", typeof(Int32));
                string Access_Token, DeviceID, MachineSerialNo;
                Access_Token = device.Device_Token;
                DeviceID = device.ThingsBoardDeviceID;
                MachineSerialNo = device.MachineSerialNo;
                int i = OSPLDEMOEntities.uspUpdateAccessToken(machineSerialNo: MachineSerialNo, aCCESS_TOKEN: Access_Token, dEVICEID: DeviceID, outRecordID: outRecordID);
                if (Convert.ToInt32(outRecordID.Value) > 0)
                    return device;
                else
                    return device;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
