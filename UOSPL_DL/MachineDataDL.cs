﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;


namespace UOSPL_DL
{
   public class MachineDataDL
    {
        OSPL_DEMOEntities OSPLDEMOEntities = null;


        public List<uspGetMachineData_Result> GetDataOfMachine(string MachineSerialNo)
        {
            Device device = new Device();
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetMachineData(machineSerialNo: MachineSerialNo).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int SaveDataOfMachine(DataOfMachine obj)
        {
            try
            {
               
                int i = 0;
                ObjectParameter outPutParam = new ObjectParameter("outRecordID", typeof(Int32));
                //ObjectParameter TB_Mother_Access_Token = new ObjectParameter("TB_Mother_Access_Token", typeof(Int32));
                outPutParam.Value = 0;

                OSPLDEMOEntities = new OSPL_DEMOEntities();

                i = OSPLDEMOEntities.uspAddUpdateMachineData
                   (
                   iD: obj.ID,
                   machineSerialNo: obj.MachineSerialNo,
                   currents: obj.Currents,
                   degree: obj.Degree,
                   speed: obj.Speed,
                fault1:obj.Fault1,
                fault2: obj.Fault2,
                fault3: obj.Fault3,
                fault4: obj.Fault4,
                fault5: obj.Fault5,
                fault6: obj.Fault6,
                   outRecordID: outPutParam
                   );

                if (outPutParam != null)
                {
                  
                    return Convert.ToInt32(outPutParam.Value);

                }
                else
                {
                    return i;
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<uspGetCustomerMachineSrNo_Result> GetCustomerMachineSrNo(int? customerID)
        {
           // Device device = new Device();
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetCustomerMachineSrNo(customerID: customerID).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
