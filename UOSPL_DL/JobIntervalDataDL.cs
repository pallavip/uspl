﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;

namespace UOSPL_DL
{
    public class JobIntervalDataDL
    {
        OSPL_DEMOEntities OSPLDEMOEntities = null;


        public List<uspGetWeldNo1_Result>GetWeldNoDetails(string actualDate)
        {
            // Device device = new Device();
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                // return OSPLDEMOEntities.uspGetCustomerMachineSrNo(customerID: customerID).ToList();
                return OSPLDEMOEntities.uspGetWeldNo1(actualDate: actualDate).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int SaveJobIntervalData(JobIntervalAllData obj)
        {
            try
            {

                int i = 0;
                ObjectParameter outPutParam = new ObjectParameter("outRecordID", typeof(Int32));
                //ObjectParameter TB_Mother_Access_Token = new ObjectParameter("TB_Mother_Access_Token", typeof(Int32));
                outPutParam.Value = 0;

                string ActDate = "";
                string ActTime = "";
                ActDate = obj.day + "/" + obj.month + "/" + obj.year;
                ActTime=obj.hrs+":"+obj.min+":" + obj.sec;

                OSPLDEMOEntities = new OSPL_DEMOEntities();

                i = OSPLDEMOEntities.uspAddJobIntervalData
                   (
                   jobIntervalID: obj.JobIntervalID,
                   machineSrNo: obj.MachineSrNo,
                   operatorCode: obj.OperatorCode,
                   //headType: obj.HeadType,
                   //controllerType: obj.ControllerType,
                //date: obj.Date,
                //time: obj.Time,
                   actualDate:ActDate,
                   actualTime: ActTime,


                weldNo: obj.WeldNo,
                tubeThickness: obj.TubeThickness,
                tubeDIA: obj.TubeDIA,
                preHeat: obj.PreHeat,
                  postFlow: obj.PostFlow,
                preFlow: obj.PreFlow,
                highPulse: obj.HighPulse,
                lowPulse: obj.LowPulse,
                 baseCurrent: obj.BaseCurrent,

                 levels: obj.Levels,
                a1: obj.A1,
                a2: obj.A2,
                a3: obj.A3,
                b1: obj.B1,
                b2: obj.B2,
                b3: obj.B3,

                c1: obj.C1,
                c2: obj.C2,
                c3: obj.C3,
                d1: obj.D1,
                d2: obj.D2,
                d3: obj.D3,

                e1: obj.E1,
                e2: obj.E2,
                e3: obj.E3,
                f1: obj.F1,
                f2: obj.F2,
                f3: obj.F3,

                 g1: obj.G1,
                g2: obj.G2,
                g3: obj.G3,
                h1: obj.H1,
                h2: obj.H2,
                h3: obj.H3,

                i1: obj.I1,
                i2: obj.I2,
                i3: obj.I3,



                   outRecordID: outPutParam
                   );

                if (outPutParam != null)
                {

                    return Convert.ToInt32(outPutParam.Value);

                }
                else
                {
                    return i;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
