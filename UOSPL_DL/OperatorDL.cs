﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;


namespace UOSPL_DL
{
   public class OperatorDL
    {
        OSPL_DEMOEntities OSPLDEMOEntities = null;
        

        public List<uspGetOperator_Result> GetOperators(int? OperatorID)
        {
            OperatorDetail operatorDetail = new OperatorDetail();
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetOperator(operatorID: OperatorID).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<uspGetOperatorWhenEdit_Result> GetOperatorsWhenEdit(int? OperatorID)
        {
            OperatorDetail operatorDetail = new OperatorDetail();
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspGetOperatorWhenEdit(operatorID: OperatorID).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int AddUpdateOperator(OperatorDetail obj)
        {
            try
            {
                //  int i = 0;
                ObjectParameter outPutParam = new ObjectParameter("outRecordID", typeof(Int32));
                //ObjectParameter TB_Mother_Access_Token = new ObjectParameter("TB_Mother_Access_Token", typeof(Int32));
                outPutParam.Value = 0;

                OSPLDEMOEntities = new OSPL_DEMOEntities();

                int i = OSPLDEMOEntities.uspAddUpdateOperator
                     (
                     operatorID: obj.OperatorID,
                     operatorName: obj.OperatorName,
                     mobileNo: obj.MobileNo,
                  
                     isActive: true,
                     createdBy: 1,
                     updatedBy: 1,
                     outRecordID: outPutParam
                    
                     );

                if (Convert.ToInt32(outPutParam.Value) > 0)
                {
                    // return Convert.ToInt32(outPutParam.Value);
                }
                return Convert.ToInt32(outPutParam.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteOperator(int? OperatorID)
        {
            try
            {
                ObjectParameter outPutParam = new ObjectParameter("outRecordID", typeof(Int32));
                int i = 0;
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                i = OSPLDEMOEntities.uspDeleteOperator(operatorID: OperatorID, outRecordID: outPutParam);
                return Convert.ToInt32(outPutParam.Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

    }
}
