﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;
using System.Data;
using System.Data.Entity.Core.Objects;

namespace UOSPL_DL
{
   public class AccountDL
    {
        OSPL_DEMOEntities OSPLDEMOEntities = null;
        /// <summary>
        /// Created By:-Sonal
        /// Creadted On:-15-11-2018
        /// Description:-User Name and User Password through authentication.
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public List<uspUserAuthentication_Result> AuthenticateUser(string UserName, string Password)
        {
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspUserAuthentication(userName: UserName, password: Password).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //  //authentication service for embedded side

      
      /// <summary>
      /// 
      /// </summary>
      /// <param name="machineID"></param>
      /// <returns></returns>
        public List<uspUnusedDevices_Result> GetUnusedDevices(int? machineID)
        {
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                return OSPLDEMOEntities.uspUnusedDevices(machineID: machineID).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveUserProfile(UserProfile userProfile)
        {
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                int i = OSPLDEMOEntities.uspSaveUserProfile(firstName: userProfile.FirstName, lastName:userProfile.LastName, email: userProfile.Email, mobileNumber: userProfile.MobileNo,  officeAddress: userProfile.OfficeAddress, pAN: userProfile.PAN, adharNumber: userProfile.Aadhar_Number, gSTNumber: userProfile.GST_Number, userName: userProfile.UserName, loginID: userProfile.LoginID);
              
                return 100;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<uspGetUserProfile1_Result> GetUserProfile(string UserName, string UserRole)
        {
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                // return OSPLDEMOEntities.uspGetUserProfile(userName: UserName, userRole: UserRole).ToList();
                return OSPLDEMOEntities.uspGetUserProfile1(userName: UserName, userRole: UserRole).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CheckMachineSrNoExist(string MachineSrNo)
        {
            try
            {
                OSPLDEMOEntities = new OSPL_DEMOEntities();
                ObjectParameter outPutParam = new ObjectParameter("isExist", typeof(Int32));
                // outPutParam.Value = 0;
                int i = OSPLDEMOEntities.uspCheckMachineSrNoExist(machineSerialNo: MachineSrNo, isExist: outPutParam);
                if (outPutParam != null)
                {
                    return Convert.ToInt32(outPutParam.Value);
                }
                else
                    return 99;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
