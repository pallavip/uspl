﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UOSPL_Model;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Security;
using System.Web.UI;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Web.Mvc.Html;

namespace UOSPL_Web.Controllers
{
    public class DeviceController : Controller
    {
        // GET: Device
        HttpClient client;
       
         public static string APIBaseAddress = "http://localhost/UOSPL_WebAPI";
      //  public static string APIBaseAddress = "http://universalapi.faralenz.in/";
        public static bool _isPersistent;
        //OSPL_DEMOEntities oSPL_DEMOEntities=new OSPL_DEMOEntities();
       // private DeviceDbContext context;
        public DeviceController()
        {
          
            client = new HttpClient();
            client.BaseAddress = new Uri(APIBaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            // _context = oSPL_DEMOEntities;
           
        }
      


        // GET: Device
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SaveDevice()
        {
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            string url = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
            HttpResponseMessage responseMessage =client.GetAsync(url).Result;

            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;


                listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                ViewBag.list = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // ViewBag.list = list;

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
               // return Json(listMachineType, JsonRequestBehavior.AllowGet);

            }
            // return RedirectToAction("GetDevices", "Device");
            return View();
            //return RedirectToAction("GetDevices");
        }

      



        // [Authorize(Roles = "Owner")]
        public async Task<ActionResult> GetDevices([DataSourceRequest]DataSourceRequest request, Device device)
        {
            try
            {
                int? machineID = device.MachineID;
                //Device device = new Device();
                //device.MachineID = MachineID;

                HttpClient client = new HttpClient();
                string url = APIBaseAddress + "/api/DeviceAPI?MachineID=" + "" + machineID;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.GetAsync(url);

                List<uspGetDevice_Result> listDevice = new List<uspGetDevice_Result>();
                if (responseMessage.IsSuccessStatusCode)
                {

                    //////// Start
                    //string draw = HttpContext.Request.Form["draw"].FirstOrDefault().ToString();
                    //// Skiping number of Rows count
                    //string start = Request.Form["start"].FirstOrDefault().ToString();
                    ////  Paging Length 10,20
                    //string length = Request.Form["length"].FirstOrDefault().ToString();
                    ////  Sort Column Name
                    //string sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault().ToString();
                    ////  Sort Column Direction(asc, desc)
                    //string sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault().ToString();
                    ////  Search Value from(Search box)
                    //string searchValue = Request.Form["search[value]"].FirstOrDefault().ToString();

                    //// Paging Size(10, 20, 50, 100)
                    //int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    //int skip = start != null ? Convert.ToInt32(start) : 0;
                    //int recordsTotal = 0;

                    ///////// End



                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    listDevice = JsonConvert.DeserializeObject<List<uspGetDevice_Result>>(responseData);
                  
                    var list = responseData;

                    //Sorting
                    //if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                    //{
                    //    listDevice = listDevice.OrderBy(sortColumn + " " + sortColumnDirection);
                    //}
                    //Search
                    //if (!string.IsNullOrEmpty(searchValue))
                    ////{
                    ////    listDevice = listDevice.Where(m => m.MachineName == searchValue || m.MachineDescription == searchValue);
                    ////}

                    ////total number of rows count 
                    //recordsTotal = listDevice.Count();
                    ////Paging 
                    //var data = listDevice.Skip(skip).Take(pageSize).ToList();
                    ////Returning Json Data

                  
                    return Json(new { data = listDevice }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    //ViewBag.Devicedetails = listDevice;
                    return View("GetDevices", Json(listDevice, JsonRequestBehavior.AllowGet));
                }
               
            }
               
            catch (Exception e)
            {
                return View("GetDevices");
            }
         
        }


        [HttpPost]
        //[Authorize(Roles = "Owner,Admin")]
        public async Task<ActionResult> SaveUpdateDevices(Device device, FormCollection form)
        {

           
            if (ModelState.IsValid)
            {

                int response = -1;
                string url = APIBaseAddress + "/api/DeviceAPI/";
                //string token = GetToken(Request);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.PostAsJsonAsync<Device>(url, device);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responsedata = responseMessage.Content.ReadAsStringAsync().Result;
                    response = Convert.ToInt16(responsedata);
                    if (response == 1)
                    {
                        TempData["Message"] = "Device Updated successfully";
                        //  return View("Distributor", distributor);
                        ModelState.Clear();
                    }
                }
            }
           
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            string url1 = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
            HttpResponseMessage responseMessage1 = client.GetAsync(url1).Result;

            if (responseMessage1.IsSuccessStatusCode)
            {
                var responseData = responseMessage1.Content.ReadAsStringAsync().Result;


                listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                ViewBag.list = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // ViewBag.list = list;

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // return Json(listMachineType, JsonRequestBehavior.AllowGet);

            }

            
           

            // RedirectToAction("GetDistributor", "Distributors");
           // return View("SaveDevice");
            return RedirectToAction("SaveDevice");
            //return View("SaveCustomer");
        }




        

        //[HttpPost]
        //[Authorize(Roles = "Owner")]

        public async Task<ActionResult> DeleteDevice(uspGetDevice_Result obj)
        {
            try
            {
                if (obj.MachineID != 0)
                {
                    //CommonUtility commonUtility = new CommonUtility();
                    //string token = commonUtility.GetToken(Request);
                    //if (token != null)
                    //{
                        int response = -1;
                        string url = APIBaseAddress + "/api/DeviceAPI/DeleteDevice/DeviceID=" + "" + obj.MachineID;
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                        HttpResponseMessage responseMessage = await client.DeleteAsync(url);
                        if (responseMessage.IsSuccessStatusCode)
                        {
                            var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                            response = Convert.ToInt16(responseData);
                            if (response > 0)
                            {
                                TempData["msg"] = "Device Deleted successfully";
                                TempData["msgType"] = "success";
                            }
                            else
                            {

                                TempData["msg"] = "Oops!something went wrong";
                                TempData["msgType"] = "danger";
                            }

                        }
                    //}
                    return RedirectToAction("DeviceMaster", "Device");
                }
                else
                {
                    return RedirectToAction("DeviceMaster", "Device");
                }
            }
            catch (Exception ex)
            {
                //ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("DeviceMaster", "Device");
            }
        }

        //[HttpDelete]
        //delete  mapping
        public async Task<ActionResult> DeleteMachine(int? MachineID)
        {

            try
            {


                Device device = new Device();
                device.MachineID = MachineID;
                ModelState.Remove("MachineID");
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                    return View("DeviceMaster");
                }
                else
                {
                    int response = -1;
                    string url = APIBaseAddress + "/api/DeviceAPI?MachineID=" + "" + MachineID;

                    //string token = GetToken(Request);
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer");
                    HttpResponseMessage responseMessage = await client.DeleteAsync(url);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responsedata = responseMessage.Content.ReadAsStringAsync().Result;
                        response = Convert.ToInt16(responsedata);
                        if (response > 0)
                        {
                            TempData["Message"] = "Device deleted successfully";

                            //get device service 
                            //HtmlHelper helper1 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMapping"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                            //helper1.RenderAction("GetDeviceIMEI");//call your action



                            //HtmlHelper helper = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMapping"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                            //helper.RenderAction("GetMappedDistributor");//call your action

                            //  RedirectToAction("GetDeviceIMEI", distributor);
                            //return View("MapDistributor", distributor);
                        }
                    }
                }
                //  RedirectToAction("GetDeviceIMEI");
                return RedirectToAction("SaveDevice", "Device");
                
            }

            catch (Exception ex)
            {

                throw;
            }
        }


        //get Machine type list
        //[HttpPost]
        //[Authorize(Roles = "Owner,Admin")]
        public async Task<ActionResult> GetMachineTypeDetails([DataSourceRequest] DataSourceRequest request)
        {
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            try
            {

                //Create URL to call WEB API
                int? MachineTypeID = 0;
                string url = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
                HttpResponseMessage responseMessage = await client.GetAsync(url);

                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;


                    listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                    ViewBag.list = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                   // ViewBag.list = list;

                    //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                    return Json(listMachineType, JsonRequestBehavior.AllowGet);

                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return Json(listMachineType, JsonRequestBehavior.AllowGet);

            //  return Json(listStates.Select(p => new { STATE_ID = p.STATE_ID, STATE_NAME = p.STATE_NAME }), JsonRequestBehavior.AllowGet);
        }

        


    }
}