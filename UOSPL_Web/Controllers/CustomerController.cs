﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UOSPL_Model;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Security;
using System.Web.UI;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Web.Mvc.Html;
using System.IO;

namespace UOSPL_Web.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        HttpClient client;
       
        public static string APIBaseAddress = "http://localhost/UOSPL_WebAPI";
       // public static string APIBaseAddress = "http://universalapi.faralenz.in/";
        public static bool _isPersistent;

        public CustomerController()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(APIBaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SaveCustomer()
        {
            List<uspGetStates_Result> listState = new List<uspGetStates_Result>();

            //Create URL to call WEB API
            int? StateID = 0;
            string url = APIBaseAddress + "/api/StateDetailsAPI/";
            // HttpResponseMessage responseMessage = await client.GetAsync(url);
            HttpResponseMessage responseMessage = client.GetAsync(url).Result;

            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;


                listState = JsonConvert.DeserializeObject<List<uspGetStates_Result>>(responseData);

                ViewBag.Statelist = new SelectList(listState, "State_ID", "State_Name");

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // return Json(listState, JsonRequestBehavior.AllowGet);

            }

            List<uspGetCities_Result> listCities = new List<uspGetCities_Result>();

            //string url = APIBaseAddress + "/api/CityDetailsAPI/";

            //HttpResponseMessage responseMessage = await client.GetAsync(APIBaseAddress + "/api/CityDetailsAPI/GetCitiDetails?StateID=" + state);
            string url1 = APIBaseAddress + "/api/CityDetailsAPI?StateID=" + "" + 0;

            HttpResponseMessage responseMessage1 = client.GetAsync(url1).Result;

            if (responseMessage1.IsSuccessStatusCode)
            {
                var responseData = responseMessage1.Content.ReadAsStringAsync().Result;
                listCities = JsonConvert.DeserializeObject<List<uspGetCities_Result>>(responseData);
                //ViewBag.listCities = listCities;
                // listCities = listCities.FindAll(e => (e.state_id == STATE_ID)).ToList();
                ViewBag.Citylist = new SelectList(listCities, "City_ID ","City_Name");

            }

            return View();
        }

        public ActionResult ViewMapping()
        {
            return View();
        }

        public ActionResult AddDistributors()
        {
            return View("SaveCustomer");
        }

        //[HttpPost]
        //public ActionResult Add(Customer customerModel)
        //{

        //}


        public ActionResult MapCustomerDevice()
        {
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            string url = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
            HttpResponseMessage responseMessage = client.GetAsync(url).Result;

            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;


                listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                ViewBag.MachineTypesList = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // ViewBag.list = list;

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // return Json(listMachineType, JsonRequestBehavior.AllowGet);

            }

            //For View bag call for Customer list
            int? customerID = 0;
            IEnumerable<uspGetCustomersDetails_Result> listCustomerList = new List<uspGetCustomersDetails_Result>();


            string url1 = APIBaseAddress + "/api/MapCusomerDetailsAPI?customerID=" + "" + customerID;

            client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
            //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DistributorID).Result;
            HttpResponseMessage responseMessagelist = client.GetAsync(url1).Result;


            if (responseMessagelist.IsSuccessStatusCode)
            {
                var responseData = responseMessagelist.Content.ReadAsStringAsync().Result;
                listCustomerList = JsonConvert.DeserializeObject<List<uspGetCustomersDetails_Result>>(responseData);
                ViewBag.CustomerList = new SelectList(listCustomerList, "CustomerID", "FirstName");

            }

            // For View bag call for Machine Serial no

            //get device service 
            //HtmlHelper helper1 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "MapCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
            //helper1.RenderAction("GetDDLMachineSerialNo");//call your action

            int? machineID = 0;
            IEnumerable<uspGetDevice_Result> listDisList = new List<uspGetDevice_Result>();


            string url2 = APIBaseAddress + "/api/MapMachineSerialNoDetailsAPI?machineID=" + "" + machineID;
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
            //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DeviceID).Result;
            HttpResponseMessage responseMessage1 = client.GetAsync(url2).Result;


            if (responseMessage1.IsSuccessStatusCode)
            {
                var responseData = responseMessage1.Content.ReadAsStringAsync().Result;
                listDisList = JsonConvert.DeserializeObject<List<uspGetDevice_Result>>(responseData);
                ViewBag.MachineSerialNoList = new SelectList(listDisList, "MachineID", "MachineSerialNo");
                // return Json(listDisList, JsonRequestBehavior.AllowGet);
            }

            return View();

        }


        //map the distributor details
        //[Authorize(Roles = "Owner,Admin")]
        [HttpPost]
        public async Task<ActionResult> MapCustomerDeviceSave(Customer customer)
        {

            ModelState.Remove("CustomerID");
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
                return RedirectToAction("MapCustomerDevice");
            }
            else
            {
                int response = -1;
                string url = APIBaseAddress + "/api/MappingDetailsAPI/";
                // string token = GetToken(Request);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.PostAsJsonAsync<Customer>(url, customer);
                if (responseMessage.IsSuccessStatusCode)
                {

                    var responsedata = responseMessage.Content.ReadAsStringAsync().Result;
                    response = Convert.ToInt16(responsedata);
                    if (response > 1)
                    {
                        TempData["Message"] = "Customer Device Mapped successfully";

                        return View("MapCustomerDevice");
                    }
                }
            }

            //For View bag call for machine type
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            string url1 = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
            HttpResponseMessage responseMessage1 = client.GetAsync(url1).Result;

            if (responseMessage1.IsSuccessStatusCode)
            {
                var responseData = responseMessage1.Content.ReadAsStringAsync().Result;


                listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                ViewBag.MachineTypesList = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // ViewBag.list = list;

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // return Json(listMachineType, JsonRequestBehavior.AllowGet);

            }

            //For View bag call for Customer list
            int? customerID = 0;
            IEnumerable<uspGetCustomersDetails_Result> listCustomerList = new List<uspGetCustomersDetails_Result>();


            string url2 = APIBaseAddress + "/api/MapCusomerDetailsAPI?customerID=" + "" + customerID;

            //client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
            //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DistributorID).Result;
            HttpResponseMessage responseMessagelist = client.GetAsync(url2).Result;


            if (responseMessagelist.IsSuccessStatusCode)
            {
                var responseData = responseMessagelist.Content.ReadAsStringAsync().Result;
                listCustomerList = JsonConvert.DeserializeObject<List<uspGetCustomersDetails_Result>>(responseData);
                ViewBag.CustomerList = new SelectList(listCustomerList, "CustomerID", "FirstName");

            }

            // For View bag call for Machine Serial no

            //get device service 
            //HtmlHelper helper1 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "MapCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
            //helper1.RenderAction("GetDDLMachineSerialNo");//call your action

            int? machineID = 0;
            IEnumerable<uspGetDevice_Result> listDisList = new List<uspGetDevice_Result>();


            string url3 = APIBaseAddress + "/api/MapMachineSerialNoDetailsAPI?machineID=" + "" + machineID;
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
            //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DeviceID).Result;
            HttpResponseMessage responseMessage2 = client.GetAsync(url3).Result;


            if (responseMessage2.IsSuccessStatusCode)
            {
                var responseData = responseMessage2.Content.ReadAsStringAsync().Result;
                listDisList = JsonConvert.DeserializeObject<List<uspGetDevice_Result>>(responseData);
                ViewBag.MachineSerialNoList = new SelectList(listDisList, "MachineID", "MachineSerialNo");
                // return Json(listDisList, JsonRequestBehavior.AllowGet);
            }

            ModelState.Clear();
            // RedirectToAction("GetMappedDistributor", "DistributorsController");
            // return View("MapCustomerDevice");
            return RedirectToAction("MapCustomerDevice");
        }

        //get Machine type list
        //[HttpPost]
        //[Authorize(Roles = "Owner,Admin")]
        public async Task<ActionResult> GetMachineTypeDetails([DataSourceRequest] DataSourceRequest request)
        {
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            try
            {

                //Create URL to call WEB API
                int? MachineTypeID = 0;
                string url = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
                HttpResponseMessage responseMessage = await client.GetAsync(url);

                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;


                    listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                    ViewBag.MachineTypesList = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                    // ViewBag.list = list;

                    //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                    return Json(listMachineType, JsonRequestBehavior.AllowGet);

                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return Json(listMachineType, JsonRequestBehavior.AllowGet);

            //  return Json(listStates.Select(p => new { STATE_ID = p.STATE_ID, STATE_NAME = p.STATE_NAME }), JsonRequestBehavior.AllowGet);
        }


        //get Machine type list
        //[HttpPost]
        //[Authorize(Roles = "Owner,Admin")]
        public async Task<ActionResult> GetCustomerDetails([DataSourceRequest] DataSourceRequest request)
        {
            List<uspGetCustomersDetails_Result> listCustomer = new List<uspGetCustomersDetails_Result>();
            try
            {

                //Create URL to call WEB API
                int? CustomerID = 0;
                string url = APIBaseAddress + "/api/MapCusomerDetailsAPI/";
                HttpResponseMessage responseMessage = await client.GetAsync(url);

                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;


                    listCustomer = JsonConvert.DeserializeObject<List<uspGetCustomersDetails_Result>>(responseData);
                    ViewBag.CustomerList = new SelectList(listCustomer, "CustomerID", "FirstName");
                    // ViewBag.list = list;

                    //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                    return Json(listCustomer, JsonRequestBehavior.AllowGet);

                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return Json(listCustomer, JsonRequestBehavior.AllowGet);

            //  return Json(listStates.Select(p => new { STATE_ID = p.STATE_ID, STATE_NAME = p.STATE_NAME }), JsonRequestBehavior.AllowGet);
        }


        //delete  mapping for customer map list
        public async Task<ActionResult> DeleteMappedCustomerDevice(Customer customer)
        {
            ModelState.Remove("CustomerID");
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
                return View("Customer");
            }
            else
            {
                int response = -1;
                string url = APIBaseAddress + "/api/MappingDetailsAPI?MachineID=" + "" + customer.MachineID;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.DeleteAsync(url);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responsedata = responseMessage.Content.ReadAsStringAsync().Result;
                    response = Convert.ToInt16(responsedata);
                    if (response > 0)
                    {
                        TempData["Message"] = "Mapping deleted successfully";

                        //get Machine type service 
                        HtmlHelper helper2 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMappedCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                        helper2.RenderAction("GetMachineTypeDetails");//call your action machine type

                        //get Search customer service 
                        HtmlHelper helper3 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMappedCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                        helper3.RenderAction("GetDDLCustomerDetails");//call your action customer detail


                        //get Machine Serial no service 
                        HtmlHelper helper1 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMappedCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                        helper1.RenderAction("GetDDLMachineSerialNo");//call your action ddl machine serial no



                        HtmlHelper helper = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMappedCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                        helper.RenderAction("GetMappedCustomerDevices");//call your action grid

                        //  RedirectToAction("GetDeviceIMEI", distributor);
                        //return View("MapDistributor", distributor);

                    }
                }
            }


            //  RedirectToAction("GetDeviceIMEI");
            // return RedirectToAction("MapCustomerDevice");
            return RedirectToAction("MapCustomerDevice", customer);
            // return RedirectToAction("SaveCustomer", "Customer");
        }


        //delete  mapping from view mapping
        public async Task<ActionResult> DeleteMappedCustomerDeviceforView(Customer customer)
        {
            ModelState.Remove("CustomerID");
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
                return View("Customer");
            }
            else
            {
                int response = -1;
                string url = APIBaseAddress + "/api/MappingDetailsAPI?MachineID=" + "" + customer.MachineID;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.DeleteAsync(url);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responsedata = responseMessage.Content.ReadAsStringAsync().Result;
                    response = Convert.ToInt16(responsedata);
                    if (response > 0)
                    {
                        TempData["Message"] = "Mapping deleted successfully";

                        ////get Machine type service 
                        //HtmlHelper helper2 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMappedCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                        //helper2.RenderAction("GetMachineTypeDetails");//call your action machine type

                        ////get Search customer service 
                        //HtmlHelper helper3 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMappedCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                        //helper3.RenderAction("GetDDLCustomerDetails");//call your action customer detail


                        ////get Machine Serial no service 
                        //HtmlHelper helper1 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMappedCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                        //helper1.RenderAction("GetDDLMachineSerialNo");//call your action ddl machine serial no



                        //HtmlHelper helper = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMappedCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                        //helper.RenderAction("GetMappedCustomerDevices");//call your action grid

                        //  RedirectToAction("GetDeviceIMEI", distributor);
                        //return View("MapDistributor", distributor);
                    }
                }
            }
            //  RedirectToAction("GetDeviceIMEI");
            return View("ViewMapping", customer);
        }



        //auto complete combobox for Customer details
        public JsonResult GetDDLCustomerDetails(string Prefix)
        {
            int? customerID = 0;
            IEnumerable<uspGetCustomersDetails_Result> listCustomerList = new List<uspGetCustomersDetails_Result>();
            try
            {

                string url = APIBaseAddress + "/api/MapCusomerDetailsAPI?customerID=" + "" + customerID;

                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DistributorID).Result;
                HttpResponseMessage responseMessage = client.GetAsync(url).Result;


                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    listCustomerList = JsonConvert.DeserializeObject<List<uspGetCustomersDetails_Result>>(responseData);
                    ViewBag.CustomerList = new SelectList(listCustomerList, "CustomerID", "FirstName");
                    // ViewBag.list = list;

                    //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                    return Json(listCustomerList, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception e)
            {
                // return View("MapDistributor");
            }
            return Json(listCustomerList, JsonRequestBehavior.AllowGet);
        }


        // autocomplete combobox for Machine Serial no
        public JsonResult GetDDLMachineSerialNo(string Prefix)
        {
            int? machineID = 0;
            IEnumerable<uspGetDevice_Result> listDisList = new List<uspGetDevice_Result>();
            try
            {

                string url2 = APIBaseAddress + "/api/MapMachineSerialNoDetailsAPI?machineID=" + "" + machineID;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DeviceID).Result;
                HttpResponseMessage responseMessage1 = client.GetAsync(url2).Result;


                if (responseMessage1.IsSuccessStatusCode)
                {
                    var responseData = responseMessage1.Content.ReadAsStringAsync().Result;
                    listDisList = JsonConvert.DeserializeObject<List<uspGetDevice_Result>>(responseData);
                    ViewBag.MachineSerialNoList = new SelectList(listDisList, "MachineID", "MachineSerialNo");
                    //return Json(listDisList, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {
                // return View("MapDistributor");
            }
            return Json(listDisList, JsonRequestBehavior.AllowGet);
        }




        // autocomplete combobox for MachineSerial no
        public JsonResult GetMachineSerialNo(string Prefix)
        {
            int? MachineID = 0;
            IEnumerable<uspGetDevice_Result> listMachineSerialList = new List<uspGetDevice_Result>();
            try
            {

                string url = APIBaseAddress + "/api/MapMachineSerialNoDetailsAPI?DeviceID=" + "" + MachineID;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DeviceID).Result;
                HttpResponseMessage responseMessage = client.GetAsync(url).Result;


                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    listMachineSerialList = JsonConvert.DeserializeObject<List<uspGetDevice_Result>>(responseData);
                    //if (!string.IsNullOrEmpty(Prefix))
                    //{
                    //    listDisList = listDisList.Where(p => p.IMEI_NO.Contains(Prefix));
                    //}
                    return Json(listMachineSerialList, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception e)
            {
                // return View("MapDistributor");
            }
            return Json(listMachineSerialList, JsonRequestBehavior.AllowGet);
        }


        //get state list
        //[HttpPost]
        //[Authorize(Roles = "Owner,Admin")]
        public async Task<ActionResult> GetStateDetails([DataSourceRequest] DataSourceRequest request)
        {
            List<uspGetStates_Result> listState = new List<uspGetStates_Result>();
            try
            {

                //Create URL to call WEB API
                int? StateID = 0;
                string url = APIBaseAddress + "/api/StateDetailsAPI/";
                HttpResponseMessage responseMessage = await client.GetAsync(url);

                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;


                    listState = JsonConvert.DeserializeObject<List<uspGetStates_Result>>(responseData);

                    ViewBag.Statelist = new SelectList(listState, "State_ID", "State_Name");

                    //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                    return Json(listState, JsonRequestBehavior.AllowGet);

                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return Json(listState, JsonRequestBehavior.AllowGet);

            //  return Json(listStates.Select(p => new { STATE_ID = p.STATE_ID, STATE_NAME = p.STATE_NAME }), JsonRequestBehavior.AllowGet);
        }



        //get cities 
        // [HttpPost]

        public async Task<ActionResult> GetCitiDetails(int? State_ID)
        {
            List<uspGetCities_Result> listCities = new List<uspGetCities_Result>();
            try
            {
                //string url = APIBaseAddress + "/api/CityDetailsAPI/";

                //HttpResponseMessage responseMessage = await client.GetAsync(APIBaseAddress + "/api/CityDetailsAPI/GetCitiDetails?StateID=" + state);
                string url1 = APIBaseAddress + "/api/CityDetailsAPI?StateID=" + "" + State_ID;

                HttpResponseMessage responseMessage = client.GetAsync(url1).Result;

                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    listCities = JsonConvert.DeserializeObject<List<uspGetCities_Result>>(responseData);
                    //ViewBag.listCities = listCities;
                    // listCities = listCities.FindAll(e => (e.state_id == STATE_ID)).ToList();
                    ViewBag.Citylist = new SelectList(listCities, "City_ID", "City_Name");
                    return Json(listCities, JsonRequestBehavior.AllowGet);




                    //Add JsonRequest behavior to allow retrieving states over http get




                    //List<uspGetStates_Result> listStates = new List<uspGetStates_Result>();

                    //listCities = listCities.Where(a => a.state_id == State_ID).ToList();
                    //ViewBag.Citylist = listCities;
                    // ViewBag.Citylist = new SelectList(listCities, "City_ID", "City_Name");

                    //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");

                    //return Json(new { data = listCities }, JsonRequestBehavior.AllowGet);
                    //  return Json(listCities.Select(p => new { CITY_ID = p.CITY_ID, CITY_NAME = p.CITY_NAME }), JsonRequestBehavior.AllowGet);
                    // return Json(listCities, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(listCities, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return Json(listCities, JsonRequestBehavior.AllowGet);
        }


        //bind the customer device details

        public async Task<ActionResult> GetMappedCustomerDevices([DataSourceRequest]DataSourceRequest request)
        {

            try
            {
                int? customerID = 0;

                string url = APIBaseAddress + "/api/MappingDetailsAPI?customerID=" + "" + customerID;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");

                HttpResponseMessage responseMessage = await client.GetAsync(url);

                List<uspGetMappedCustomerDevice_Result> listDevice = new List<uspGetMappedCustomerDevice_Result>();
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    listDevice = JsonConvert.DeserializeObject<List<uspGetMappedCustomerDevice_Result>>(responseData);

                    var list = responseData;

                    return Json(new { data = listDevice }, JsonRequestBehavior.AllowGet);

                }

                List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
                string url1 = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
                HttpResponseMessage responseMessage1 = client.GetAsync(url1).Result;

                if (responseMessage1.IsSuccessStatusCode)
                {
                    var responseData = responseMessage1.Content.ReadAsStringAsync().Result;


                    listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                    ViewBag.MachineTypesList = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                    // ViewBag.list = list;

                    //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                    // return Json(listMachineType, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return View("MapCustomerDevice", Json(listDevice, JsonRequestBehavior.AllowGet));
                }

                //return Json(new { data = listDevice }, JsonRequestBehavior.AllowGet);
                return RedirectToAction("MapCustomerDevice");
            }
            catch (Exception)
            {

                return View("MapDistributor");
            }
        }




        //[Authorize(Roles = "Owner,Admin")]
        public async Task<ActionResult> GetCustomer([DataSourceRequest]DataSourceRequest request, Customer customer)
        {
            try
            {
                int? customerID = customer.CustomerID;

                HttpClient client = new HttpClient();

                string url = APIBaseAddress + "/api/CustomerAPI?customerID=" + "" + customerID;

                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.GetAsync(url);
                //List<uspGetCustomersDetails_Result> listDevice = new List<uspGetCustomersDetails_Result>();
                List<Customer> listCustomer = new List<Customer>();
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    listCustomer = JsonConvert.DeserializeObject<List<Customer>>(responseData);

                    var list = responseData;
                    //DataSourceResult result = listCustomer.ToDataSourceResult(request);
                    //return Json(result, JsonRequestBehavior.AllowGet);
                    return Json(new { data = listCustomer }, JsonRequestBehavior.AllowGet);


                }
                else
                {
                    return View("GetCustomer", Json(listCustomer, JsonRequestBehavior.AllowGet));
                }

            }
            catch (Exception e)
            {
                return View("GetCustomer");

            }
        }


        //[HttpPost]
        //[Authorize(Roles = "Owner")]
        public async Task<ActionResult> DeleteCustomer(uspGetCustomersDetails_Result obj)
        {
            try
            {
                if (obj.CustomerID != 0)
                {
                    //CommonUtility commonUtility = new CommonUtility();
                    //string token = commonUtility.GetToken(Request);
                    //if (token != null)
                    //{
                    int response = -1;
                    string url = APIBaseAddress + "/api/CustomerAPI/DeleteCustomer?CustomerID=" + "" + obj.CustomerID;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                    HttpResponseMessage responseMessage = await client.DeleteAsync(url);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        response = Convert.ToInt16(responseData);
                        if (response > 0)
                        {
                            TempData["msg"] = "Customer Deleted successfully";
                            TempData["msgType"] = "success";
                        }
                        else
                        {

                            TempData["msg"] = "Oops!something went wrong";
                            TempData["msgType"] = "danger";
                        }

                    }
                    //}
                    return RedirectToAction("CustomerMaster", "Customer");
                }
                else
                {
                    return RedirectToAction("CustomerMaster", "Customer");
                }
            }
            catch (Exception ex)
            {
                //ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("CustomerMaster", "Customer");
            }
        }


        //[HttpDelete]
        //delete  mapping
        public async Task<ActionResult> DeleteCustomers(int? CustomerID)
        {

            try
            {
                Customer customer = new Customer();
                customer.CustomerID = CustomerID;
                ModelState.Remove("CustomerID");
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                    return View("CustomerMaster");
                }
                else
                {
                    int response = -1;
                    string url = APIBaseAddress + "/api/CustomerAPI?CustomerID=" + "" + CustomerID;

                    //string token = GetToken(Request);
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer");
                    HttpResponseMessage responseMessage = await client.DeleteAsync(url);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responsedata = responseMessage.Content.ReadAsStringAsync().Result;
                        response = Convert.ToInt16(responsedata);
                        if (response > 0)
                        {
                            TempData["Message"] = "Device deleted successfully";

                            //get device service 
                            //HtmlHelper helper1 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMapping"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                            //helper1.RenderAction("GetDeviceIMEI");//call your action



                            //HtmlHelper helper = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMapping"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                            //helper.RenderAction("GetMappedDistributor");//call your action

                            //  RedirectToAction("GetDeviceIMEI", distributor);
                            //return View("MapDistributor", distributor);
                        }
                    }
                }
                //  RedirectToAction("GetDeviceIMEI");
                return RedirectToAction("SaveCustomer", "Customer");

            }

            catch (Exception ex)
            {

                throw;
            }
        }



        [HttpPost]
        public async Task<ActionResult> SaveUpdateCustomer(Customer obj)
        {
            int response = -1;
            try
            {
                if (obj.FirstName != null && obj.MobileNo != null && obj.OfficeAddress != null && obj.EmailID != null && obj.GST_Number != null)
                {
                    response = -1;


                    string url = APIBaseAddress + "/api/CustomerAPI";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync<Customer>(url, obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        response = Convert.ToInt16(responseData);
                        if (response == 2)
                        {
                            TempData["msgType"] = "success";
                            TempData["message"] = "Customer Updated Successfully.";
                            //  ViewBag.JavaScriptFunction = string.Format("showSuccessToast('{0}');", "true");
                        }
                        else if (response == 1)
                        {
                            TempData["msgType"] = "success";
                            TempData["message"] = "Customer Added Successfully.";
                            //  ViewBag.JavaScriptFunction = string.Format("showSuccessToast('{0}');", "true");
                        }
                        else
                        {
                            TempData["msgType"] = "Error";
                            TempData["message"] = "OOps!!! something went wrong.Try agin or contact to Admin";
                            //   ViewBag.JavaScriptFunction = string.Format("showSuccessToast('{0}');", "true");

                        }
                    }

                    return RedirectToAction("SaveCustomer", "Customer");
                }
            }

            catch (Exception ex)
            {
                //ExceptionLogging.SendErrorToText(ex);
                //return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("SaveCustomer", "Customer");
        }

        //Created new by sonal on 15/10/2018
        [HttpPost]
        //[Authorize(Roles = "Owner,Admin")]
        public async Task<ActionResult> SaveCustomerDetails(Customer customer, FormCollection form, HttpPostedFileBase ImageFile)
        {

            ModelState.Remove("CustomerID");
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
                return View("Customer");
            }
            else
            {
                string path = "";
                var fileName = "";
                var extension = "";
                if (ImageFile != null)
                {
                    fileName = Path.GetFileNameWithoutExtension(ImageFile.FileName);
                    extension = Path.GetExtension(ImageFile.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    customer.Company_Logo_Path = "~/DocumentUploded/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/DocumentUploded/"), fileName);
                    ImageFile.SaveAs(fileName);
                }
                int response = -1;
                string url = APIBaseAddress + "/api/CustomerAPI";
                //string token = GetToken(Request);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.PostAsJsonAsync<Customer>(url, customer);
                if (responseMessage.IsSuccessStatusCode)
                {

                    var responsedata = responseMessage.Content.ReadAsStringAsync().Result;
                    response = Convert.ToInt16(responsedata);
                    if (response >= 1)
                    {
                        //file upload code
                        // ImageFile.SaveAs(fileName);

                        TempData["Message"] = "Customer Updated successfully";
                        //  return View("Distributor", distributor);
                    }
                }
            }

            List<uspGetStates_Result> listState = new List<uspGetStates_Result>();

            //Create URL to call WEB API
            int? StateID = 0;
            string url2 = APIBaseAddress + "/api/StateDetailsAPI/";
            // HttpResponseMessage responseMessage = await client.GetAsync(url);
            HttpResponseMessage responseMessage2 = client.GetAsync(url2).Result;

            if (responseMessage2.IsSuccessStatusCode)
            {
                var responseData = responseMessage2.Content.ReadAsStringAsync().Result;


                listState = JsonConvert.DeserializeObject<List<uspGetStates_Result>>(responseData);

                ViewBag.Statelist = new SelectList(listState, "State_ID", "State_Name");

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // return Json(listState, JsonRequestBehavior.AllowGet);

            }

            List<uspGetCities_Result> listCities = new List<uspGetCities_Result>();

            //string url = APIBaseAddress + "/api/CityDetailsAPI/";

            //HttpResponseMessage responseMessage = await client.GetAsync(APIBaseAddress + "/api/CityDetailsAPI/GetCitiDetails?StateID=" + state);
            string url1 = APIBaseAddress + "/api/CityDetailsAPI?StateID=" + "" + 0;

            HttpResponseMessage responseMessage1 = client.GetAsync(url1).Result;

            if (responseMessage1.IsSuccessStatusCode)
            {
                var responseData = responseMessage1.Content.ReadAsStringAsync().Result;
                listCities = JsonConvert.DeserializeObject<List<uspGetCities_Result>>(responseData);
                //ViewBag.listCities = listCities;
                // listCities = listCities.FindAll(e => (e.state_id == STATE_ID)).ToList();
                ViewBag.Citylist = new SelectList(listCities, "City_ID ", "City_Name");

            }


            

            ModelState.Clear();
            // RedirectToAction("GetDistributor", "Distributors");
           // return View("SaveCustomer");
            return RedirectToAction("SaveCustomer");
        }
    }
}
