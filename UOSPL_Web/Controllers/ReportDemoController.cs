﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using UOSPL_Web.DataSet;
using System.Web.Mvc.Html;
using Microsoft.Reporting.WebForms;


namespace UOSPL_Web.Controllers
{
    public class ReportDemoController : Controller
    {
        // GET: ReportDemo
        public ActionResult Index()
        {
            MachineDataSet ds = new MachineDataSet();
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(900);
            reportViewer.Height = Unit.Percentage(900);
            var connectionString = ConfigurationManager.ConnectionStrings["OSPL_DEMOEntities"].ConnectionString;


            SqlConnection conx = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("SELECT * FROM Customer", conx);

            adp.Fill(ds, ds.Customer.TableName);


            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\Report1.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("MachineDataSet", ds.Tables[0]));
            ViewBag.ReportViewer = reportViewer;

            return View();
        }
    }
}