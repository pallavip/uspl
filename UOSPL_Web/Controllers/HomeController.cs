﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UOSPL_Model;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Security;
using System.Web.UI;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Web.Mvc.Html;

namespace UOSPL_Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Device
        HttpClient client;
      
         public static string APIBaseAddress = "http://localhost/UOSPL_WebAPI";
       // public static string APIBaseAddress = "http://universalapi.faralenz.in/";
        public static bool _isPersistent;
        //OSPL_DEMOEntities oSPL_DEMOEntities=new OSPL_DEMOEntities();
        // private DeviceDbContext context;
        public HomeController()
        {

            client = new HttpClient();
            client.BaseAddress = new Uri(APIBaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            // _context = oSPL_DEMOEntities;

        }


        [Authorize(Roles = "Customer,Admin")]
        public ActionResult Home()
        {
            return View("Home");
        }

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DashboardDisplay()
        {
            int customerID1 = Convert.ToInt32(TempData["UserID"]);
            //For View bag call
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            string url1 = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
            HttpResponseMessage responseMessage1 = client.GetAsync(url1).Result;

            if (responseMessage1.IsSuccessStatusCode)
            {
                var responseData = responseMessage1.Content.ReadAsStringAsync().Result;


                listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                ViewBag.list = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // ViewBag.list = list;

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // return Json(listMachineType, JsonRequestBehavior.AllowGet);

            }

            return View();
        }

        //get Machine type list
        //[HttpPost]
        //[Authorize(Roles = "Owner,Admin")]
        public async Task<ActionResult> GetMachineTypeDetails([DataSourceRequest] DataSourceRequest request)
        {
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            try
            {

                //Create URL to call WEB API
                int? MachineTypeID = 0;
                string url = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
                HttpResponseMessage responseMessage = await client.GetAsync(url);

                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;


                    listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                    ViewBag.list = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                    // ViewBag.list = list;

                    //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                    return Json(listMachineType, JsonRequestBehavior.AllowGet);

                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return Json(listMachineType, JsonRequestBehavior.AllowGet);

            //  return Json(listStates.Select(p => new { STATE_ID = p.STATE_ID, STATE_NAME = p.STATE_NAME }), JsonRequestBehavior.AllowGet);
        }

        public ActionResult HealthMonitoringDashboard()
        {

            //int customerID2 = 0;
           var customerID2 = TempData.Peek("UserID");
            //For View bag call"token.TmpCustomerID"            //List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            //string url1 = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
            //HttpResponseMessage responseMessage1 = client.GetAsync(url1).Result;

            //if (responseMessage1.IsSuccessStatusCode)
            //{
            //    var responseData = responseMessage1.Content.ReadAsStringAsync().Result;


            //    listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
            //    ViewBag.list = new SelectList(listMachineType, "MachineTypeID", "Machi

            //    //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
            //    // return Json(listMachineType, JsonRequestBehavior.AllowGeneTypeName");
            //    // ViewBag.list = list;t);

            //}

            // For viewbag machine serail no.
            // int? machineID = 0;
            IEnumerable<uspGetDevice_Result> listDisList = new List<uspGetDevice_Result>();


            string url2 = APIBaseAddress + "/api/CustomerMachineSrNoAPI?customerID=" + "" + customerID2;
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
            //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DeviceID).Result;
            HttpResponseMessage responseMessage2 = client.GetAsync(url2).Result;


            if (responseMessage2.IsSuccessStatusCode)
            {
                var responseData = responseMessage2.Content.ReadAsStringAsync().Result;
                listDisList = JsonConvert.DeserializeObject<List<uspGetDevice_Result>>(responseData);
                ViewBag.MachineSerialNoList = new SelectList(listDisList, "MachineSerialNo", "MachineSerialNo");
                // return Json(listDisList, JsonRequestBehavior.AllowGet);
            }


            return View();
        }

        public ActionResult CustomerDashboardDisplay()
        {
            //For View bag call
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            string url1 = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
            HttpResponseMessage responseMessage1 = client.GetAsync(url1).Result;

            if (responseMessage1.IsSuccessStatusCode)
            {
                var responseData = responseMessage1.Content.ReadAsStringAsync().Result;


                listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                ViewBag.list = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // ViewBag.list = list;

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // return Json(listMachineType, JsonRequestBehavior.AllowGet);

            }


            // For View bag call for Machine Serial no

            

            int? machineID = 0;
            IEnumerable<uspGetDevice_Result> listDisList = new List<uspGetDevice_Result>();


            string url2 = APIBaseAddress + "/api/MapMachineSerialNoDetailsAPI?machineID=" + "" + machineID;
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
            //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DeviceID).Result;
            HttpResponseMessage responseMessage2 = client.GetAsync(url2).Result;


            if (responseMessage2.IsSuccessStatusCode)
            {
                var responseData = responseMessage2.Content.ReadAsStringAsync().Result;
                listDisList = JsonConvert.DeserializeObject<List<uspGetDevice_Result>>(responseData);
                ViewBag.MachineSerialNoList = new SelectList(listDisList, "MachineID", "MachineSerialNo");
                // return Json(listDisList, JsonRequestBehavior.AllowGet);
            }


            // For count of total device

            List<uspDeviceCountDetails_Result> listDevice = new List<uspDeviceCountDetails_Result>();
            int? customerID = 0;

            customerID =Convert.ToInt32(TempData["UserID"]);

            TempData["UserID"] = customerID;

            string url3 = APIBaseAddress + "/api/CountDeviceAPI?CustomerID=" + "" + customerID;
           // client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
            HttpResponseMessage responseMessage3 =  client.GetAsync(url3).Result;

            if (responseMessage3.IsSuccessStatusCode)
            {
                var responseData = responseMessage3.Content.ReadAsStringAsync().Result;

                uspDeviceCountDetails_Result listDevice1 = JsonConvert.DeserializeObject<uspDeviceCountDetails_Result>(responseData);

                ViewBag.OfflineDeviceCount = listDevice1.OfflineDeviceCount;        
                ViewBag.OnlineDeviceCount = listDevice1.OnlineDeviceCount;
                ViewBag.TotalDeviceCount = listDevice1.TotalDeviceCount;
                ViewBag.OfflineDeviceCount48 = listDevice1.OfflineDeviceCount48;


            }
            else
            {
                //ViewBag.Devicedetails = listDevice;
                // return View("GetDevices", Json(listDevice, JsonRequestBehavior.AllowGet));
            }

            //return View("MapCustomerDevice");


            // For More info of device

            ////////List<uspGetDashBoardDetails_Result> DashboardDetailsMoreInfo = new List<uspGetDashBoardDetails_Result>();
          
            ////////string url4 = APIBaseAddress + "/api/DashBoardDetailsMoreInfoAPI?CustomerID=" + "" + customerID +"&"+"Flag=" +""+1;
            ////////// client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
            ////////HttpResponseMessage responseMessage4 = client.GetAsync(url4).Result;

            ////////if (responseMessage4.IsSuccessStatusCode)
            ////////{
            ////////    var responseData = responseMessage4.Content.ReadAsStringAsync().Result;

            ////////    DashboardDetailsMoreInfo = JsonConvert.DeserializeObject<List<uspGetDashBoardDetails_Result>>(responseData);
            ////////    return Json(DashboardDetailsMoreInfo, JsonRequestBehavior.AllowGet);


            ////////}
            ////////else
            ////////{
            ////////    //ViewBag.Devicedetails = listDevice;
            ////////    // return View("GetDevices", Json(listDevice, JsonRequestBehavior.AllowGet));
            ////////}

            //////////return View("MapCustomerDevice");

            return View();
        }

        //for more info
        //public async Task<ActionResult> DashboardMoreInfoDetails([DataSourceRequest] DataSourceRequest request)
        //{

        //}




            // autocomplete combobox for Machine Serial no
            public JsonResult GetDDLMachineSerialNo(string Prefix)
        {
            int? machineID = 0;
            IEnumerable<uspGetDevice_Result> listDisList = new List<uspGetDevice_Result>();
            try
            {

                string url2 = APIBaseAddress + "/api/MapMachineSerialNoDetailsAPI?machineID=" + "" + machineID;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DeviceID).Result;
                HttpResponseMessage responseMessage1 = client.GetAsync(url2).Result;


                if (responseMessage1.IsSuccessStatusCode)
                {
                    var responseData = responseMessage1.Content.ReadAsStringAsync().Result;
                    listDisList = JsonConvert.DeserializeObject<List<uspGetDevice_Result>>(responseData);
                    ViewBag.MachineSerialNoList = new SelectList(listDisList, "MachineID", "MachineSerialNo");
                    //return Json(listDisList, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {
                // return View("MapDistributor");
            }
            return Json(listDisList, JsonRequestBehavior.AllowGet);
        }





    }
}