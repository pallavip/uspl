﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UOSPL_Model;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Security;
using System.Web.UI;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Web.Mvc.Html;
using System.Configuration;
using System.Data.SqlClient;
using UOSPL_Web.DataSet;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;

namespace UOSPL_Web.Controllers
{
    public class CustomerUserController : Controller
    {
        // GET: Device
        HttpClient client;
      
        public static string APIBaseAddress = "http://localhost/UOSPL_WebAPI";
       // public static string APIBaseAddress = "http://universalapi.faralenz.in/";
        public static bool _isPersistent;
        //OSPL_DEMOEntities oSPL_DEMOEntities=new OSPL_DEMOEntities();
        // private DeviceDbContext context;
        public CustomerUserController()
        {

            client = new HttpClient();
            client.BaseAddress = new Uri(APIBaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            // _context = oSPL_DEMOEntities;

        }


        // GET: CustomerUser
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ViewReport()
        {
            var customerID2 = TempData.Peek("UserID");

            //For View bag call
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            string url1 = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
            HttpResponseMessage responseMessage1 = client.GetAsync(url1).Result;

            if (responseMessage1.IsSuccessStatusCode)
            {
                var responseData = responseMessage1.Content.ReadAsStringAsync().Result;


                listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                ViewBag.list = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // ViewBag.list = list;

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // return Json(listMachineType, JsonRequestBehavior.AllowGet);

            }


            // For View bag call for Machine Serial no

           IEnumerable<uspGetDevice_Result> listDisList = new List<uspGetDevice_Result>();


            string url2 = APIBaseAddress + "/api/CustomerMachineSrNoAPI?customerID=" + "" + customerID2;
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
            //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DeviceID).Result;
            HttpResponseMessage responseMessage2 = client.GetAsync(url2).Result;


            if (responseMessage2.IsSuccessStatusCode)
            {
                var responseData = responseMessage2.Content.ReadAsStringAsync().Result;
                listDisList = JsonConvert.DeserializeObject<List<uspGetDevice_Result>>(responseData);
                ViewBag.MachineSerialNoList = new SelectList(listDisList, "MachineSerialNo", "MachineSerialNo");
                // return Json(listDisList, JsonRequestBehavior.AllowGet);
            }

            //For View bag call for weld no
            
            string  SelectedDate="";

            List<uspGetWeldNo1_Result> ListWeldNo = new List<uspGetWeldNo1_Result>();
            string url3 = APIBaseAddress + "/api/WeldNoDetailsAPI?ActualDate=" + "" + SelectedDate;
            HttpResponseMessage responseMessage3 = client.GetAsync(url3).Result;

            if (responseMessage3.IsSuccessStatusCode)
            {
                var responseData = responseMessage3.Content.ReadAsStringAsync().Result;


                ListWeldNo = JsonConvert.DeserializeObject<List<uspGetWeldNo1_Result>>(responseData);
                ViewBag.WeldNoList = new SelectList(ListWeldNo, "WeldNo", "WeldNo");
                // ViewBag.list = list;

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // return Json(listMachineType, JsonRequestBehavior.AllowGet);

            }

            MachineDataSet ds = new MachineDataSet();
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(900);
            reportViewer.Height = Unit.Percentage(900);
            var connectionString = ConfigurationManager.ConnectionStrings["OSPL_DEMOEntities"].ConnectionString;


            SqlConnection conx = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("select j.ProjectDoneBy,j.CustomerName,od.OperatorName,j.OrbitalHeadType,jid.MachineSrNo, jid.WeldNo,jid.ActualDate,jid.ActualTime,jid.TubeThickness,jid.TubeDIA,jid.PreHeat,jid.BaseCurrent,jid.PostFlow,jid.PreFlow,jid.HighPulse,jid.LowPulse,jds.Levels,jds.Degree,jds.Speed,jds.Currents,JID.JobIntervalID  from Device d left join MachineType m  on d.MachineTypeDeviceID=m.MachineTypeID left join JobDetails j on d.MachineTypeDeviceID=j.MachineTypeID right join JobIntervalData jid on jid.MachineSrNo=d.MachineSerialNo left join JobIntervalDataDetails jds on jid.JobIntervalID=jds.JobIntervalID left join  OperatorDetails od on jid.OperatorCode=od.OperatorID   where  jid.MachineSrNo='ORB-7K-001' and jid.ActualDate='20/11/2018' and jid.WeldNo=12 or jid.WeldNo=28 ", conx);

            adp.Fill(ds, ds.Customer.TableName);


            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\Report2.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("JobDetailsData", ds.Tables[0]));
            //  reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds.Tables[0]));
            ViewBag.ReportViewer = reportViewer;

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CustomerViewReport(JobIntervalData obj)
        {
 
            return RedirectToAction("ViewReport", "CustomerUser");
        }

            // Save operator
            public ActionResult Operators()
        {
            return View();
        }

        public ActionResult JobDetails()
        {
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            string url = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
            HttpResponseMessage responseMessage = client.GetAsync(url).Result;

            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;


                listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                ViewBag.MachineTypesList = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // ViewBag.list = list;

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // return Json(listMachineType, JsonRequestBehavior.AllowGet);

            }

            

            // For View bag call for Machine Serial no

            //get device service 
            //HtmlHelper helper1 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "MapCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
            //helper1.RenderAction("GetDDLMachineSerialNo");//call your action

            int? machineID = 0;
            IEnumerable<uspGetDevice_Result> listDisList = new List<uspGetDevice_Result>();


            string url2 = APIBaseAddress + "/api/MapMachineSerialNoDetailsAPI?machineID=" + "" + machineID;
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
            //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DeviceID).Result;
            HttpResponseMessage responseMessage1 = client.GetAsync(url2).Result;


            if (responseMessage1.IsSuccessStatusCode)
            {
                var responseData = responseMessage1.Content.ReadAsStringAsync().Result;
                listDisList = JsonConvert.DeserializeObject<List<uspGetDevice_Result>>(responseData);
                ViewBag.MachineSerialNoList = new SelectList(listDisList, "MachineID", "MachineSerialNo");
                // return Json(listDisList, JsonRequestBehavior.AllowGet);
            }

            return View();
        }



        [HttpPost]
        //[Authorize(Roles = "Owner,Admin")]
        public async Task<ActionResult> SaveUpdateOperator(OperatorDetail operatorDetail, FormCollection form)
        {

            ModelState.Remove("OperatorID");
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
                return View("Operators");
            }
            else
            {

                int response = -1;
                string url = APIBaseAddress + "/api/OperatorAPI/";
                //string token = GetToken(Request);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.PostAsJsonAsync<OperatorDetail>(url, operatorDetail);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responsedata = responseMessage.Content.ReadAsStringAsync().Result;
                    response = Convert.ToInt16(responsedata);
                    if (response == 1)
                    {
                        TempData["Message"] = "Operator Updated successfully";
                        //  return View("Distributor", distributor);
                    }
                }
            }
            

            ModelState.Clear();


            // RedirectToAction("GetDistributor", "Distributors");
            // return View("SaveDevice");
            return RedirectToAction("Operators");
            //return View("SaveCustomer");
        }



        // [Authorize(Roles = "Owner")]
        public async Task<ActionResult> GetOperators([DataSourceRequest]DataSourceRequest request, OperatorDetail operatorDetail)
        {
            try
            {
                int? operatorID = operatorDetail.OperatorID;
                //Device device = new Device();
                //device.MachineID = MachineID;

                HttpClient client = new HttpClient();
                string url = APIBaseAddress + "/api/OperatorAPI?OperatorID=" + "" + operatorID;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.GetAsync(url);

                List<OperatorDetail> listOperator = new List<OperatorDetail>();
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    listOperator = JsonConvert.DeserializeObject<List<OperatorDetail>>(responseData);

                    var list = responseData;

                    return Json(new { data = listOperator }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    //ViewBag.Devicedetails = listDevice;
                    return View("GetOperators", Json(listOperator, JsonRequestBehavior.AllowGet));
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }

        }


        // [Authorize(Roles = "Owner")]
        public async Task<ActionResult> GetOperatorsWhenEdit([DataSourceRequest]DataSourceRequest request, OperatorDetail operatorDetail)
        {
            try
            {
                int? operatorID = operatorDetail.OperatorID;
                //Device device = new Device();
                //device.MachineID = MachineID;

                HttpClient client = new HttpClient();
                string url = APIBaseAddress + "/api/OperatorWhenEditAPI?OperatorID=" + "" + operatorID;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.GetAsync(url);

                List<OperatorDetail> listOperator = new List<OperatorDetail>();
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    listOperator = JsonConvert.DeserializeObject<List<OperatorDetail>>(responseData);

                    var list = responseData;

                    return Json(new { data = listOperator }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    //ViewBag.Devicedetails = listDevice;
                    return View("GetOperators", Json(listOperator, JsonRequestBehavior.AllowGet));
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }

        }


        //[HttpDelete]
        //delete  mapping
        public async Task<ActionResult> DeleteOperator(int? OperatorID)
        {

            try
            {


                OperatorDetail operatorDetail = new OperatorDetail();
                operatorDetail.OperatorID = OperatorID;
                ModelState.Remove("OperatorID");
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                    return View("DeviceMaster");
                }
                else
                {
                    int response = -1;
                    string url = APIBaseAddress + "/api/OperatorAPI?OperatorID=" + "" + OperatorID;

                    //string token = GetToken(Request);
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer");
                    HttpResponseMessage responseMessage = await client.DeleteAsync(url);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responsedata = responseMessage.Content.ReadAsStringAsync().Result;
                        response = Convert.ToInt16(responsedata);
                        if (response > 0)
                        {
                            TempData["Message"] = "Operator deleted successfully";

                            //get device service 
                            //HtmlHelper helper1 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMapping"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                            //helper1.RenderAction("GetDeviceIMEI");//call your action



                            //HtmlHelper helper = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMapping"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                            //helper.RenderAction("GetMappedDistributor");//call your action

                            //  RedirectToAction("GetDeviceIMEI", distributor);
                            //return View("MapDistributor", distributor);
                        }
                    }
                }
                //  RedirectToAction("GetDeviceIMEI");
                return RedirectToAction("Operators", "CustomerUser");

            }

            catch (Exception ex)
            {

                throw;
            }
        }

        //Job detail forms

        //map the distributor details
        //[Authorize(Roles = "Owner,Admin")]
        [HttpPost]
        public async Task<ActionResult> JobDetailsSave(JobDetail jobDetail, FormCollection form)
        {

            ModelState.Remove("JobDetailID");
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
                return View("JobDetails");
            }
            else
            {
                int response = -1;
                string url = APIBaseAddress + "/api/JobDetailsAPI/";
                // string token = GetToken(Request);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.PostAsJsonAsync<JobDetail>(url, jobDetail);
                if (responseMessage.IsSuccessStatusCode)
                {

                    var responsedata = responseMessage.Content.ReadAsStringAsync().Result;
                    response = Convert.ToInt16(responsedata);
                    if (response == 1)
                    {
                        TempData["Message"] = "Job Details added successfully";

                        return View("JobDetails");
                    }
                }
            }

            //For View bag call for machine type
            List<uspGetMachineType_Result> listMachineType = new List<uspGetMachineType_Result>();
            string url1 = APIBaseAddress + "/api/MachineTypeDetailsAPI/";
            HttpResponseMessage responseMessage1 = client.GetAsync(url1).Result;

            if (responseMessage1.IsSuccessStatusCode)
            {
                var responseData = responseMessage1.Content.ReadAsStringAsync().Result;


                listMachineType = JsonConvert.DeserializeObject<List<uspGetMachineType_Result>>(responseData);
                ViewBag.MachineTypesList = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // ViewBag.list = list;

                //ViewBag.listMachineType = new SelectList(listMachineType, "MachineTypeID", "MachineTypeName");
                // return Json(listMachineType, JsonRequestBehavior.AllowGet);

            }

           

            // For View bag call for Machine Serial no

            //get device service 
            //HtmlHelper helper1 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "MapCustomerDevice"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
            //helper1.RenderAction("GetDDLMachineSerialNo");//call your action

            int? machineID = 0;
            IEnumerable<uspGetDevice_Result> listDisList = new List<uspGetDevice_Result>();


            string url3 = APIBaseAddress + "/api/MapMachineSerialNoDetailsAPI?machineID=" + "" + machineID;
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
            //HttpResponseMessage responseMessage = client.PostAsJsonAsync(url, DeviceID).Result;
            HttpResponseMessage responseMessage2 = client.GetAsync(url3).Result;


            if (responseMessage2.IsSuccessStatusCode)
            {
                var responseData = responseMessage2.Content.ReadAsStringAsync().Result;
                listDisList = JsonConvert.DeserializeObject<List<uspGetDevice_Result>>(responseData);
                ViewBag.MachineSerialNoList = new SelectList(listDisList, "MachineID", "MachineSerialNo");
                // return Json(listDisList, JsonRequestBehavior.AllowGet);
            }

            ModelState.Clear();
            // RedirectToAction("GetMappedDistributor", "DistributorsController");
            // return View("MapCustomerDevice");
            return RedirectToAction("JobDetails");
        }



        // [Authorize(Roles = "Owner")]
        public async Task<ActionResult> GetJobDetails([DataSourceRequest]DataSourceRequest request, JobDetail jobDetail)
        {
            try
            {
                int? jobDetailID = jobDetail.JobDetailID;
                //Device device = new Device();
                //device.MachineID = MachineID;

                HttpClient client = new HttpClient();
                string url = APIBaseAddress + "/api/JobDetailsAPI?JobDetailID=" + "" + jobDetailID;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer ");
                HttpResponseMessage responseMessage = await client.GetAsync(url);

                List<JobDetail> listJobDetail = new List<JobDetail>();
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    listJobDetail = JsonConvert.DeserializeObject<List<JobDetail>>(responseData);

                    var list = responseData;

                    return Json(new { data = listJobDetail }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    //ViewBag.Devicedetails = listDevice;
                    return View("GetJobDetails", Json(listJobDetail, JsonRequestBehavior.AllowGet));
                }

            }

            catch (Exception e)
            {
                return View("GetJobDetails");
            }

        }


        //[HttpDelete]
        //delete  mapping
        public async Task<ActionResult> DeleteJobDetails(int? JobDetailID)
        {

            try
            {


                
               
                ModelState.Remove("JobDetailID");
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                    return View("JobDetailMaster");
                }
                else
                {
                    int response = -1;
                    string url = APIBaseAddress + "/api/JobDetailsAPI?JobDetailID=" + "" + JobDetailID;

                    //string token = GetToken(Request);
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer");
                    HttpResponseMessage responseMessage = await client.DeleteAsync(url);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responsedata = responseMessage.Content.ReadAsStringAsync().Result;
                        response = Convert.ToInt16(responsedata);
                        if (response > 0)
                        {
                            TempData["Message"] = "Job Detail deleted successfully";

                            //get device service 
                            //HtmlHelper helper1 = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMapping"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                            //helper1.RenderAction("GetDeviceIMEI");//call your action



                            //HtmlHelper helper = new HtmlHelper(new ViewContext(ControllerContext, new WebFormView(ControllerContext, "DeleteMapping"), new ViewDataDictionary(), new TempDataDictionary(), new System.IO.StringWriter()), new ViewPage());
                            //helper.RenderAction("GetMappedDistributor");//call your action

                            //  RedirectToAction("GetDeviceIMEI", distributor);
                            //return View("MapDistributor", distributor);
                        }
                    }
                }
                //  RedirectToAction("GetDeviceIMEI");
                return RedirectToAction("JobDetails", "CustomerUser");

            }

            catch (Exception ex)
            {

                throw;
            }
        }



    }
}