﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using UOSPL_Model;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Web.Security;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.Owin.Security;
using System.Net;

namespace UOSPL_Web.Controllers
{
    public class AccountController : Controller
    {
        
        HttpClient client;
       
        public static string APIBaseAddress = "http://localhost/UOSPL_WebAPI";
      //  public static string APIBaseAddress = "http://universalapi.faralenz.in/";
        public static bool _isPersistent;
        //OSPL_DEMOEntities oSPL_DEMOEntities=new OSPL_DEMOEntities();
        // private DeviceDbContext context;
        public AccountController()
        {

            client = new HttpClient();
            client.BaseAddress = new Uri(APIBaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            // _context = oSPL_DEMOEntities;

        }
        //
        // GET: /Account/
        // [Authorize]
        public ActionResult Login()
        {
            string token = GetToken(Request);
            if (token != "")
            {
              //  return RedirectToAction("DashboardDisplay", "Home");

            }
            
            return View("Login");
           
        }

        private string GetToken(HttpRequestBase Request)
        {
            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null && authCookie.Value != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                if (authTicket != null && authTicket.UserData != null)
                {
                    string[] UserDetails = authTicket.UserData.Split(';');
                    if (UserDetails != null)
                    {
                        return UserDetails[0];
                    }
                }
            }
            return "";
        }


        private void GetUserInfo(string token)
        {
            var jwtHandler = new JwtSecurityTokenHandler();
            // var jwtInput = txtJwtIn.Text;

            //Check if readable token (string is in a JWT format)
            var readableToken = jwtHandler.CanReadToken(token);
            readableToken = true;
            if (readableToken != true)
            {
                // txtJwtOut.Text = "The token doesn't seem to be in a proper JWT format.";
            }
            if (readableToken == true)
            {
                var tok = jwtHandler.ReadJwtToken(token);

                //Extract the headers of the JWT
                var headers = tok.Header;
                var jwtHeader = "{";
                foreach (var h in headers)
                {
                    jwtHeader += '"' + h.Key + "\":\"" + h.Value + "\",";
                }
                jwtHeader += "}";
                // txtJwtOut.Text = "Header:\r\n" + JToken.Parse(jwtHeader).ToString(Formatting.Indented);

                //Extract the payload of the JWT
                var claims = tok.Claims;
                var jwtPayload = "{";
                foreach (Claim c in claims)
                {
                    jwtPayload += '"' + c.Type + "\":\"" + c.Value + "\",";
                }
                jwtPayload += "}";
                // txtJwtOut.Text += "\r\nPayload:\r\n" + JToken.Parse(jwtPayload).ToString(Formatting.Indented);
            }
        }


        [HttpPost]
       // [Authorize(Roles = "Customer,Admin")]
        public async Task<ActionResult> Login(Login Login, IEnumerable<bool> chkRememberMe)
        {
            string response = "";
            if (string.IsNullOrEmpty(Login.UserName))
            {
                ModelState.AddModelError("UserName", "User Name is required.");
            }
            if (string.IsNullOrEmpty(Login.Password))
            {
                ModelState.AddModelError("Password", "Password is required.");
            }
            if (!ModelState.IsValid)
            {
                return View("Login");
            }
            else
            {
               
                
                // The URL of the WEB API Service
                HttpResponseMessage responseMessage = await AuthenticateUser(Login);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    var token = JsonConvert.DeserializeObject<Token>(responseData);

                    if (token.access_token != "")
                    {
                        string url = APIBaseAddress + "/api/Value/Get";
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                        HttpResponseMessage result = await client.GetAsync(url);
                        //responseData = result.Content.ReadAsStringAsync().Result;
                        //var claims = JsonConvert.DeserializeObject<Claim>(responseData);
                        //string username = Login.UserName + " " + Login.Password;
                        string userdata = token.access_token + ";" + token.name + ";" + token.role.Trim() + ";" + token.username + ";" + Login.Password + ";" + token.TmpCustomerID;
                        if (chkRememberMe != null && chkRememberMe.Count() == 2)
                        {
                            _isPersistent = true;
                            SignInRemember(userdata, true);
                        }
                        else
                        {
                            _isPersistent = false;
                            //SignInRemember(userdata, false);
                        }
                       var roletype=token.role.Trim();
                        TempData["UserID"] = token.TmpCustomerID;

                        
                       
                        TempData.Keep();
                        if (roletype == "Admin")
                        {
                            return RedirectToAction("DashboardDisplay", "Home");
                        }
                        else
                        {
                            
                                return RedirectToAction("CustomerDashboardDisplay", "Home");
                                                        

                        }
                       // return RedirectToAction("Home", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "The user name or password is incorrect");
                        TempData["msg"] = "Please enter valid username and passowrd";
                        TempData["msgType"] = "danger";
                        return View("Login");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "The user name or password is incorrect");
                    TempData["msg"] = "Please enter valid username and passowrd";
                    TempData["msgType"] = "danger";
                    return View("Login");
                }
                return View("Login");
            }
        }

      

        private void SignInRemember(string token, bool isPersistent = false)
        {
            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null && authCookie.Value != null)
            {
                FormsAuthentication.SignOut();
            }
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, "UserName", DateTime.Now, DateTime.Now.AddMinutes(30), isPersistent, token, FormsAuthentication.FormsCookiePath);

            string encTicket = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            if (isPersistent)
                cookie.Expires = DateTime.Now.AddDays(10);
            Response.Cookies.Add(cookie);
            //  Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));
        }

        //[OverrideAuthentication]       
        //[AllowAnonymous]
        //[Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<HttpResponseMessage> AuthenticateUser(Login Login)
        {
            string url = APIBaseAddress + "/token";
            HttpResponseMessage result;
            HttpContent content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", Login.UserName),
                    new KeyValuePair<string, string>("password", Login.Password)
                });
            result = client.PostAsync(url, content).Result;
            return result;
        }

        public ActionResult Logout()
        {
            //Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [Authorize(Roles = "Owner,Admin")]
        public async Task<ActionResult> SaveProfile(UserProfile userProfile)
        {
            if (!ModelState.IsValid)
            {
                return View("Profile");
            }
            else
            {
                int response = -1;
                string url = APIBaseAddress + "/api/AccountAPI/SaveUserProfile";
                string token = GetToken(Request);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                HttpResponseMessage responseMessage = await client.PostAsJsonAsync<UserProfile>(url, userProfile);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    response = Convert.ToInt16(responseData);
                    if (response == 1)
                    {
                        return View("Profile", userProfile);
                    }
                }
            }
            return View("Profile", userProfile);
        }


       
        public async Task<ActionResult> GetProfile()
        {
            try
            {

                var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null && authCookie.Value != null)
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    if (authTicket != null && authTicket.UserData != null)
                    {
                        string[] UserDetails = authTicket.UserData.Split(';');
                        if (UserDetails != null)
                        {
                            Token tok = new Token();
                            tok.username = UserDetails[3];
                            tok.role = UserDetails[2];
                            string url = APIBaseAddress + "/api/AccountAPI/GetUserProfile";
                            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + UserDetails[0]);
                            HttpResponseMessage responseMessage = await client.PostAsJsonAsync<Token>(url, tok);
                            List<UserProfile> listUser = new List<UserProfile>();
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                                listUser = JsonConvert.DeserializeObject<List<UserProfile>>(responseData);
                                //uspGetUserProfile_Result objUser = new uspGetUserProfile_Result();
                                //objUser = listUser[0];
                                if (listUser.Count > 0)
                                {
                                    return View("Profile", listUser[0]);
                                }
                            }
                            else if (responseMessage.ReasonPhrase == "Unauthorized")
                            {
                                Login login = new Login();
                                login.UserName = UserDetails[3];
                                login.Password = UserDetails[4];
                                HttpResponseMessage result = await AuthenticateUser(login);
                                if (result.IsSuccessStatusCode)
                                {
                                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                                    var token = JsonConvert.DeserializeObject<Token>(responseData);

                                    if (token.access_token != "")
                                    {
                                        string userdata = token.access_token + ";" + token.name + ";" + token.role + ";" + token.username + ";" + login.Password;
                                        SignInRemember(userdata, _isPersistent);
                                        authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                                        if (authCookie != null && authCookie.Value != null)
                                        {
                                            authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                                            if (authTicket != null && authTicket.UserData != null)
                                            {
                                                UserDetails = authTicket.UserData.Split(';');
                                                if (UserDetails != null)
                                                {
                                                    tok = new Token();
                                                    tok.username = UserDetails[3];
                                                    tok.role = UserDetails[2];
                                                    url = APIBaseAddress + "/api/AccountAPI/GetUserProfile";
                                                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + UserDetails[0]);
                                                    responseMessage = await client.PostAsJsonAsync<Token>(url, tok);
                                                    if (responseMessage.IsSuccessStatusCode)
                                                    {
                                                        responseData = responseMessage.Content.ReadAsStringAsync().Result;
                                                        listUser = JsonConvert.DeserializeObject<List<UserProfile>>(responseData);
                                                        if (listUser.Count > 0)
                                                        {
                                                            return View("Profile", listUser[0]);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return View("Profile");
            }
            catch (Exception e)
            {
                return View("Login");
            }
        }



        //new service for embedded for get token

        //[HttpPost]
        //public async Task<ActionResult> EmbLogin(EMB_Login Login)
        //{
        //    if (string.IsNullOrEmpty(Login.IMEI_NO))
        //    {
        //        ModelState.AddModelError("UserName", "User Name is required.");
        //    }
        //    if (string.IsNullOrEmpty(Login.MANUFACTURING_ID))
        //    {
        //        ModelState.AddModelError("Password", "Password is required.");
        //    }
        //    if (!ModelState.IsValid)
        //    {
        //        return View("Login");
        //    }
        //    else
        //    {
        //        // The URL of the WEB API Service
        //        HttpResponseMessage responseMessage = await EMBAuthenticateUser(Login);
        //        if (responseMessage.IsSuccessStatusCode)
        //        {
        //            var responseData = responseMessage.Content.ReadAsStringAsync().Result;
        //            var token = JsonConvert.DeserializeObject<Token>(responseData);

        //            if (token.access_token != "")
        //            {
        //                string url = APIBaseAddress + "/api/Value/Get";
        //                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
        //                HttpResponseMessage result = await client.GetAsync(url);

        //                // string userdata = token.access_token + ";" + token.Name + ";" + token.Role + ";" + token.UserName + ";" + Login.Password;

        //                return RedirectToAction("Home", "Home");
        //            }
        //            else
        //            {
        //                ModelState.AddModelError(string.Empty, "The user name or password is incorrect");
        //                TempData["msg"] = "Please enter valid username and passowrd";
        //                TempData["msgType"] = "danger";
        //                return View("Login");
        //            }
        //        }
        //        else
        //        {
        //            ModelState.AddModelError(string.Empty, "The user name or password is incorrect");
        //            TempData["msg"] = "Please enter valid username and passowrd";
        //            TempData["msgType"] = "danger";
        //            return View("Login");
        //        }
        //        return View("Login");
        //    }
        //    return View("Login");
        //}

        //public async Task<HttpResponseMessage> EMBAuthenticateUser(EMB_Login Login)
        //{
        //    string url = APIBaseAddress + "/EMBtoken";
        //    HttpResponseMessage result;

        //    HttpContent content = new FormUrlEncodedContent(new[]
        //        {
        //            new KeyValuePair<string, string>("grant_type", "password"),
        //            new KeyValuePair<string, string>("IMEI_NO", Login.IMEI_NO),
        //            new KeyValuePair<string, string>("MANUFACTURING_ID", Login.MANUFACTURING_ID)
        //        });
        //    result = client.PostAsync(url, content).Result;
        //    return result;
        //}


        // GET: Account
        public ActionResult Index()
        {
            return View();
        }


        //public ActionResult Login(Login login)
        //{
        //    if (string.IsNullOrEmpty(login.UserName))
        //    {
        //        ModelState.AddModelError("UserName", "User Name is required.");
        //    }
        //    else if (login.UserName == "admin" && login.Password == "admin")
        //    {
        //        return RedirectToAction("DashboardDisplay", "Home");
        //    }
        //    else if (login.UserName == "customer" && login.Password == "customer")
        //    {
        //        return RedirectToAction("CustomerDashboardDisplay", "Home");
        //    }
        //    else
        //    {
        //        ModelState.AddModelError("UserName", "User Name and password is required.");
        //    }
        //    return View();
        //    // return RedirectToAction("DashboardDisplay", "Home");
        //}

        // Machine Sr No exist or not

        //[Authorize(Roles = "Customer,Admin")]
        [HttpPost]
        public async Task<JsonResult> CheckMachineSrNoExist(Device device)
        {
            int response = -1;

            string result;
            if (string.IsNullOrEmpty(device.MachineSerialNo))
            {
                ModelState.AddModelError("MachineSerialNo", "Machine Serial No is required.");
            }
            else
            {
                string url = APIBaseAddress + "/api/CheckDeviceExistAPI";
               
                string token = GetToken(Request);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                HttpResponseMessage responseMessage = await client.PostAsJsonAsync<Device>(url, device);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    response = Convert.ToInt16(responseData);
                    if (response == 1)
                    {
                        ModelState.AddModelError("MachineSerialNo", "Machine Serial No. not available");
                        result = "Machine Serial No. not available";
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            result = "Machine Serial No. is available";
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}