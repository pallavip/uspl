﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace UOSPL_Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)

        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        //let us take out the username now                
                        FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                        if (authTicket != null && authTicket.UserData != null)
                        {
                            string[] UserDetails = authTicket.UserData.Split(';');
                            if (UserDetails != null)
                            {
                                string username = UserDetails[1];
                                string role = UserDetails[2].ToString();
                                //Let us set the Pricipal with our user specific details
                                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                                            new System.Security.Principal.GenericIdentity(username, "Forms"), role.Split(' '));
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //somehting went wrong
                    }
                }
            }
        }
    }
}
