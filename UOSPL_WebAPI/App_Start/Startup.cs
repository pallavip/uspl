﻿using UOSPL_WebAPI.Provider;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[assembly: OwinStartup(typeof(UOSPL_WebAPI.App_Start.Startup))]
namespace UOSPL_WebAPI.App_Start
{
    public class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {

            var OAuthOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(10080),
                Provider = new SimpleAuthorizationServerProvider()
            };
            var OAuthOptions1 = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/EMBtoken"),
                // AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(10080),
                //Provider = new SimpleAuthorizationServerProviderEMB()
            };



            // app.UseOAuthBearerTokens(OAuthOptions);
            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthAuthorizationServer(OAuthOptions1);

            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }

        public void Configuration(IAppBuilder app)
        {


            ConfigureAuth(app);
            // GlobalConfiguration.Configure(WebApiConfig.Register);
            var config = new HttpConfiguration();
            WebApiConfig.Register(config);
            // app.UseWebApi(config);
            app.UseWebApi(config);

        }
    }
}