﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_Model;
using UOSPL_BL;

namespace UOSPL_WebAPI.Controllers
{
    public class MachineTypeDetailsAPIController : ApiController
    {
        //service for get MachineType list
        [HttpGet]
        public List<uspGetMachineType_Result> GetMachineTypeDetails()

        {
            try
            {
                DeviceBL deviceBL = new DeviceBL();
                return deviceBL.GetMachineTypes();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
