﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_Model;
using UOSPL_BL;

namespace UOSPL_WebAPI.Controllers
{
    public class MapCusomerDetailsAPIController : ApiController
    {
        // [Authorize]

        [HttpGet]
        public List<uspGetCustomersDetails_Result> GetCustomerDetails(int? customerID)
        {
            try
            {

                CustomerBL objBL = new CustomerBL();
                return objBL.GetCustomerDetails(customerID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
