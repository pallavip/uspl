﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_Model;
using UOSPL_BL;

namespace UOSPL_WebAPI.Controllers
{
    public class WeldNoDetailsAPIController : ApiController
    {
        [HttpGet]
        // [Authorize]
        public List<uspGetWeldNo1_Result> GetWeldNoDetails(string actualDate)
        {
            try
            {
                JobIntervalDataBL objBL = new JobIntervalDataBL();
                return objBL.GetWeldNoDetails(actualDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
