﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;


namespace UOSPL_WebAPI.Controllers
{
    public class CustomerAPIController : ApiController
    {

        // [Authorize]
        [HttpGet]
        // [HttpGet]     
        public List<uspGetCustomersDetails_Result> GetCustomerDetails(int? customerID)
        {
            try
            {

                CustomerBL objBL = new CustomerBL();
                return objBL.GetCustomerDetails(customerID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpDelete]
        // [Authorize]
        public int DeleteCustomer(int? CustomerID)
        {
            try
            {
                CustomerBL objBL = new CustomerBL();
                return objBL.DeleteCustomer(CustomerID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        //   [Authorize]
        public int SaveCustomer(Customer obj)
        {
            try
            {
                CustomerBL objBL = new CustomerBL();
                return objBL.AddUpdateCustomer(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
