﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;


namespace UOSPL_WebAPI.Controllers
{
    public class DeviceInfoSaveAPIController : ApiController
    {

        [HttpPost]
        public Device GetThingsBoardAccessToken(Device device)
        {
            try
            {
                GetTBAccessTokenBL getTBAccessTokenBL = new GetTBAccessTokenBL();
                return getTBAccessTokenBL.GetThingsBoardAccessToken(device);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
