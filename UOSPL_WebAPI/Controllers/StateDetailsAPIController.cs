﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_Model;
using UOSPL_BL;

namespace UOSPL_WebAPI.Controllers
{
    public class StateDetailsAPIController : ApiController
    {
        //service for get state list
        [HttpGet]
        public List<uspGetStates_Result> GetStateDetails()
        {
            try
            {
                CustomerBL customerBL = new CustomerBL();
                return customerBL.GetStates();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
