﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_Model;
using UOSPL_BL;
using Newtonsoft.Json;

namespace UOSPL_WebAPI.Controllers
{
    public class MappingDetailsAPIController : ApiController
    {
        [HttpGet]
        //[Authorize]
        public List<uspGetMappedCustomerDevice_Result> GetMappedCustomerDevices(int? CustomerID)
        {
            try
            {
                CustomerBL objBL = new CustomerBL();
                return objBL.GetMappedCustomerDevices(CustomerID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        //   [Authorize]
        public int MapCustomerDevice(Customer customer)
        {
            try
            {
                CustomerBL objBL = new CustomerBL();
                return objBL.MapCustomerDevice(customer);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //delete mapping

        [HttpDelete]
        //  [Authorize]
        public int DeleteMappedCustomerDevice(int MachineID)
        {
            try
            {
                CustomerBL objBL = new CustomerBL();
                return objBL.DeleteMappedCustomerDevice(MachineID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
