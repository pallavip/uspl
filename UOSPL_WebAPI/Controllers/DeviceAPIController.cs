﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;



namespace UOSPL_WebAPI.Controllers
{
    public class DeviceAPIController : ApiController
    {


        //[HttpGet]
        //public Device GetDevices(int? MachineID)
        //{
        //    Device device = new Device();
        //    return device;
        //}


        //[Authorize]
        [HttpGet]
        public List<uspGetDevice_Result> GetDevices(int? MachineID)
        {
            //  [FromBody]
            // int MachineID = 1;

            try
            {
                DeviceBL objBL = new DeviceBL();
                return objBL.GetDevices(MachineID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpDelete]
       // [Authorize]
        public int DeleteDevice(int? MachineID)
        {
           
            try
            {
                DeviceBL objBL = new DeviceBL();
                return objBL.DeleteDevice(MachineID);
              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
     //   [Authorize]
        public int SaveDevices(Device obj)
        {
            try
            {
                DeviceBL objBL = new DeviceBL();
                return objBL.AddUpdateDevice(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
