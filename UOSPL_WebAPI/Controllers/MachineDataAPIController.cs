﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;


namespace UOSPL_WebAPI.Controllers
{
    public class MachineDataAPIController : ApiController
    {
        //[Authorize]
        [HttpGet]
        public List<uspGetMachineData_Result> GetDataOfMachine(string MachineSerialNo)
        {
            //  [FromBody]
            // int MachineID = 1;

            try
            {
                MachineDataBL objBL = new MachineDataBL();
                return objBL.GetDataOfMachine(MachineSerialNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        //   [Authorize]
        public int SaveDataOfMachine(DataOfMachine obj)
        {
            try
            {
                MachineDataBL objBL = new MachineDataBL();
                return objBL.SaveDataOfMachine(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
