﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;

namespace UOSPL_WebAPI.Controllers
{
    public class OperatorWhenEditAPIController : ApiController
    {
        //[Authorize]
        [HttpGet]
        public List<uspGetOperatorWhenEdit_Result> GetOperatorsWhenEdit(int? OperatorID)
        {
            //  [FromBody]
            // int MachineID = 1;

            try
            {
                OperatorBL objBL = new OperatorBL();
                return objBL.GetOperatorsWhenEdit(OperatorID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
