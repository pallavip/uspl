﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;


namespace UOSPL_WebAPI.Controllers
{
    public class CountDeviceAPIController : ApiController
    {
       
        [HttpGet]
       // [Authorize]
        public CustomerDeviceCount GetDevicesCount(int? CustomerID)
        {
            try
            {
                // int OwnerID = 1; int DeviceID=123;
                DeviceBL objBL = new DeviceBL();
                return objBL.GetDevicesCount(CustomerID);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
