﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_Model;
using UOSPL_BL;

namespace UOSPL_WebAPI.Controllers
{
    public class MapMachineSerialNoDetailsAPIController : ApiController
    {
        [HttpGet]
        // [Authorize]
        public List<uspUnusedDevices_Result> GetUnusedDevices(int? machineID)
        {
            try
            {
                AccountBL objBL = new AccountBL();
                return objBL.GetUnusedDevices(machineID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
