﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;

namespace UOSPL_WebAPI.Controllers
{
    public class OperatorAPIController : ApiController
    {
        //[Authorize]
        [HttpGet]
        public List<uspGetOperator_Result> GetOperators(int? OperatorID)
        {
            //  [FromBody]
            // int MachineID = 1;

            try
            {
                OperatorBL objBL = new OperatorBL();
                return objBL.GetOperators(OperatorID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        //   [Authorize]
        public int SaveOperator(OperatorDetail obj)
        {
            try
            {
                OperatorBL objBL = new OperatorBL();
                return objBL.AddUpdateOperator(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpDelete]
        // [Authorize]
        public int DeleteOperator(int? OperatorID)
        {

            try
            {
                OperatorBL objBL = new OperatorBL();
                return objBL.DeleteOperator(OperatorID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
