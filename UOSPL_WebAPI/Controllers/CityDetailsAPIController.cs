﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_Model;
using UOSPL_BL;

namespace UOSPL_WebAPI.Controllers
{
    public class CityDetailsAPIController : ApiController
    {
        //service  for get city list
        // [Authorize]
        [HttpGet]
        public List<uspGetCities_Result> GetCitiDetails(int? StateID)
        {
            try
            {
                CustomerBL customerBL = new CustomerBL();
                return customerBL.GetCities(StateID);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
