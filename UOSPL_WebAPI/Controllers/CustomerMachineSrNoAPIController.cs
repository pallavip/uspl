﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_Model;
using UOSPL_BL;

namespace UOSPL_WebAPI.Controllers
{
    public class CustomerMachineSrNoAPIController : ApiController
    {
        [HttpGet]
        // [Authorize]
        public List<uspGetCustomerMachineSrNo_Result> GetCustomerMachineSrNo(int? customerID)
        {
            try
            {
                MachineDataBL objBL = new MachineDataBL();
                return objBL.GetCustomerMachineSrNo(customerID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
