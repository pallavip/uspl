﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;

namespace UOSPL_WebAPI.Controllers
{
    public class JobDetailsAPIController : ApiController
    {
        [HttpGet]
        public List<uspGetJobDetails_Result> GetJobDetails(int? JobDetailID)
        {
            //  [FromBody]
            // int MachineID = 1;

            try
            {
                JobDetailsBL objBL = new JobDetailsBL();
                return objBL.GetJobDetails(JobDetailID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        //   [Authorize]
        public int SaveJobDetail(JobDetail obj)
        {
            try
            {
                JobDetailsBL objBL = new JobDetailsBL();
                return objBL.AddUpdateJobDetail(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpDelete]
        // [Authorize]
        public int DeleteJobDetail(int? JobDetailID)
        {

            try
            {
                JobDetailsBL objBL = new JobDetailsBL();
                return objBL.DeleteJobDetail(JobDetailID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
