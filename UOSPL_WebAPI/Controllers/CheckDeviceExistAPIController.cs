﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;
//using UOSPL_WebAPI.ActionFilter;

namespace UOSPL_WebAPI.Controllers
{
    public class CheckDeviceExistAPIController : ApiController
    {
        [HttpPost]
        [AllowAnonymous]
        public int CheckMachineSrNoExist(Device device)
        {

            try
            {
                AccountBL objBL = new AccountBL();
                return objBL.CheckMachineSrNoExist(device.MachineSerialNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
