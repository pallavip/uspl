﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;

namespace UOSPL_WebAPI.Controllers
{
    public class JobIntervalDataAPIController : ApiController
    {
        [HttpPost]
        //   [Authorize]
        public int SaveJobIntervalData(JobIntervalAllData obj)
        {
            try
            {
                JobIntervalDataBL objBL = new JobIntervalDataBL();
                return objBL.SaveJobIntervalData(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
