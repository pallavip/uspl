﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;

namespace UOSPL_WebAPI.Controllers
{
    public class DashBoardDetailsMoreInfoAPIController : ApiController
    {
        [HttpGet]
        public List<uspGetDashBoardDetails_Result> GetDashboardDetailsMoreInfo(int? CustomerID, int Flag)
        {
            try
            {
                CustomerBL customerBL = new CustomerBL();
                return customerBL.GetDashboardDetailsMoreInfo(CustomerID, Flag);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
