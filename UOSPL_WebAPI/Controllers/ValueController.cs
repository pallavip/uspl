﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Cors;


namespace UOSPL_WebAPI.Controllers
{
    //[Authorize]
    [EnableCors("*", "*", "*")]
    public class ValueController : ApiController
    {
        public IHttpActionResult Get()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;

            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            return Ok(claims);
        }

        public List<string> strings = new List<string>()
          {
          "val","val1","val2"

          };

        // // [HttpGet]
        //  public IEnumerable<string> Get()
        //  {
        //      Console.WriteLine("Get");
        //      return strings;

        //  }
        [HttpGet]
        public string Get(int id)
        {
            Console.WriteLine("Get");
            return strings[id];
        }
        [HttpPost]
        public void Post([FromBody]string value)
        {
            strings.Add(value);
        }

        [System.Web.Http.HttpPut]
        //[HttpPut]

        public IHttpActionResult Put(int id, [FromBody]string value)
        {
            strings[id] = value;
            return Ok();
        }

        [HttpDelete]
        public void Delete(int id)
        {
            strings.RemoveAt(id);
        }
    }
}
