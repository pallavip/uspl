﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using UOSPL_BL;
using UOSPL_Model;
//using UOSPL_WebAPI.ActionFilter;

namespace UOSPL_WebAPI.Controllers
{
    public class AccountAPIController : ApiController
    {
        [HttpPost]
        [Authorize]
        public List<uspGetUserProfile1_Result> GetUserProfile(Token token)
        {
            try
            {
                AccountBL objBL = new AccountBL();
                return objBL.GetUserProfile(token.username, token.role);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Authorize]
        public int SaveUserProfile(UserProfile userProfile)
        {
            try
            {
                AccountBL objBL = new AccountBL();
                return objBL.SaveUserProfile(userProfile);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public int CheckMachineSrNoExist(Device device)
        {

            try
            {
                AccountBL objBL = new AccountBL();
                return objBL.CheckMachineSrNoExist(device.MachineSerialNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
