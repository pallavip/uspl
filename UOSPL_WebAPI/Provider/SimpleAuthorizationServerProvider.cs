﻿using UOSPL_BL;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Cors;
using UOSPL_Model;
using UOSPL_WebAPI.Controllers;
//using UOSPL_WebAPI.ActionFilter;
using System.Net;

namespace UOSPL_WebAPI.Provider
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    //[APIActionFilter]
    //[APIExceptionFilter]
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        Token oken = new Token();
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); //   
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            AccountBL objBL = new AccountBL();
            List<uspUserAuthentication_Result> UsersList = new List<uspUserAuthentication_Result>();
            UsersList = objBL.AuthenticateUser(context.UserName, context.Password);

            uspUserAuthentication_Result UserDetails = null;
            if (UsersList.Count > 0)
            {
                UserDetails = new uspUserAuthentication_Result()
                {
                    Name = UsersList[0].Name,
                    IsValid = UsersList[0].IsValid,
                    UserName = UsersList[0].UserName,
                    UserType = UsersList[0].UserType,
                    TmpCustomerID=UsersList[0].TmpCustomerID,

                };
            }

            

            if (UserDetails != null)
            {
                if (UserDetails.IsValid == 1)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, UserDetails.UserType));

                    var props = new AuthenticationProperties(new Dictionary<string, string>
                    {
                        {
                            "role",UserDetails.UserType
                        },
                        {
                            "name",UserDetails.Name
                        },
                        {
                            "username", UserDetails.UserName
                        },

                        {
                            "tmpCustomerID", UserDetails.TmpCustomerID.ToString()
                        },

                        {
                            "success", "True"
                        },
                        {
                            "message", "Login Successfully."
                        },
                      

                    });

                    var ticket = new AuthenticationTicket(identity, props);
                    context.Validated(ticket);
                }
                else
                {
                    //string jsonString = "{\"message\": \"The email or password is incorrect.\"}";
                    //context.SetError(new string(' ', jsonString.Length - 12));
                    //context.Response.StatusCode = 400;
                    //context.Response.Write(jsonString);
                    //context.SetError("Invalid_Grant12", "Provided username and password are wrong");
                    //context.Rejected();

                    context.Rejected();

                    //set the error message
                    context.SetError("Invalid Username or Password", "");


                    //add a new key in the header along with the statusCode you'd like to return
                    context.Response.Headers.Add("Change_Status_Code", new[] { ((int)HttpStatusCode.InternalServerError).ToString() });
                    return;

                }
            }
            else
            {
                //string jsonString = "{\"message\": \"The email or password is incorrect.\"}";
                //context.SetError(new string(' ', jsonString.Length - 12));
                //context.Response.StatusCode = 400;
                //context.Response.Write(jsonString);
                context.Rejected();

                //set the error message
                context.SetError("Invalid UserName or Password", "Invalid UserName or Password");

                //add a new key in the header along with the statusCode you'd like to return
                context.Response.Headers.Add("Change_Status_Code", new[] { ((int)HttpStatusCode.InternalServerError).ToString() });
                return;
            }

            return;

        }


        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {

            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            // context.AdditionalResponseParameters.Add("add", "user");
            return Task.FromResult<object>(null);
        }


    }
}