﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;
using UOSPL_DL;

namespace UOSPL_BL
{
   public class MachineDataBL
    {
        public List<uspGetMachineData_Result> GetDataOfMachine(string MachineSerialNo)
        {
            try
            {
                MachineDataDL objDL = new MachineDataDL();
                return objDL.GetDataOfMachine(MachineSerialNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveDataOfMachine(DataOfMachine obj)
        {
            try
            {
                MachineDataDL objDL = new MachineDataDL();
                return objDL.SaveDataOfMachine(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<uspGetCustomerMachineSrNo_Result> GetCustomerMachineSrNo(int? customerID)
        {
            try
            {
                MachineDataDL objDL = new MachineDataDL();
                return objDL.GetCustomerMachineSrNo(customerID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
