﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;
using UOSPL_DL;

namespace UOSPL_BL
{
    public class JobIntervalDataBL
    {
        public int SaveJobIntervalData(JobIntervalAllData obj)
        {
            try
            {
                JobIntervalDataDL objDL = new JobIntervalDataDL();
                return objDL.SaveJobIntervalData(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Get Weld No.
        public List<uspGetWeldNo1_Result> GetWeldNoDetails(string actualDate)
        {
            try
            {
                JobIntervalDataDL objDL = new JobIntervalDataDL();
                return objDL.GetWeldNoDetails(actualDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
