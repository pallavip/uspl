﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;
using UOSPL_DL;

namespace UOSPL_BL
{
   public class CustomerBL
    {

        public List<uspGetCustomersDetails_Result> GetCustomerDetails(int? customerID)
        {
            try
            {
                CustomerDL objDL = new CustomerDL();
                return objDL.GetCustomerDetails(customerID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<uspGetDashBoardDetails_Result> GetDashboardDetailsMoreInfo(int? customerID, int flag)
        {
            try
            {
                CustomerDL objDL = new CustomerDL();
                return objDL.GetDashboardDetailsMoreInfo(customerID, flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int MapCustomerDevice(Customer customer)
        {
            try
            {
                CustomerDL objDL = new CustomerDL();
                return objDL.MapCustomerDevice(customer);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //delete mapping
        public int DeleteMappedCustomerDevice(int MachineID)
        {
            try
            {
                CustomerDL objDL = new CustomerDL();
                return objDL.DeleteMappedCustomerDevice(MachineID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //GET MAPPED CUSTOMER DEVICE LIST
        public List<uspGetMappedCustomerDevice_Result> GetMappedCustomerDevices(int? CustomerID)
        {
            try
            {
                CustomerDL objDL = new CustomerDL();
                return objDL.GetMappedCustomerDevices(CustomerID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddUpdateCustomer(Customer obj)
        {
            try
            {
                CustomerDL objDL = new CustomerDL();
                return objDL.AddUpdateCustomer(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int DeleteCustomer(int? CustomerID)
        {
            try
            {
                CustomerDL objDL = new CustomerDL();
                return objDL.DeleteCustomer(CustomerID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //get state details
        public List<uspGetStates_Result> GetStates()
        {
            try
            {
                CustomerDL objDL = new CustomerDL();
                return objDL.GetStates();
            }
            catch (Exception)
            {

                throw;
            }
        }

        //get  cities details

        public List<uspGetCities_Result> GetCities(int? StateID)
        {
            try
            {
                CustomerDL objDL = new CustomerDL();
                return objDL.GetCities(StateID);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
