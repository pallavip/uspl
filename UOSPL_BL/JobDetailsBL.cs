﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;
using UOSPL_DL;

namespace UOSPL_BL
{
   public class JobDetailsBL
    {

        public List<uspGetJobDetails_Result>GetJobDetails(int? JobDetailID)
        {
            try
            {
                JobDetailsDL objDL = new JobDetailsDL();
                return objDL.GetJobDetails(JobDetailID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddUpdateJobDetail(JobDetail obj)
        {
            try
            {
                JobDetailsDL objDL = new JobDetailsDL();
                return objDL.AddUpdateJobDetail(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteJobDetail(int? JobDetailID)
        {
            try
            {
                JobDetailsDL objDL = new JobDetailsDL();
                return objDL.DeleteJobDetail(JobDetailID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
