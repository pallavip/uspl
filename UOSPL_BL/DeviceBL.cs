﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;
using UOSPL_DL;

namespace UOSPL_BL
{
  public class DeviceBL
    {
        public List<uspGetDevice_Result> GetDevices(int? MachineID)
        {
            try
            {
                DeviceDL objDL = new DeviceDL();
                return objDL.GetDevices(MachineID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



      


        public CustomerDeviceCount GetDevicesCount(int? CustomerID)
        {
            try
            {
                DeviceDL objDL = new DeviceDL();
                return objDL.GetDevicesCount(CustomerID);
            }
            catch (Exception)
            {

                throw;
            }
        }
        //get Macine Type details
        public List<uspGetMachineType_Result> GetMachineTypes()
        {
            try
            {
                DeviceDL objDL = new DeviceDL();
                return objDL.GetMachineTypes();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int AddUpdateDevice(Device obj)
        {
            try
            {
                DeviceDL objDL = new DeviceDL();
                return objDL.AddUpdateDevice(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteDevice(int? MachineID)
        {
            try
            {
                DeviceDL objDL = new DeviceDL();
                return objDL.DeleteDevice(MachineID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
