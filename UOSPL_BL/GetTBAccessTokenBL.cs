﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;
using UOSPL_DL;

namespace UOSPL_BL
{
  public  class GetTBAccessTokenBL
    {
        public Device GetThingsBoardAccessToken(Device device)
        {

            try
            {
                GetTBAccessTokenDL getTBAccessTokenBL = new GetTBAccessTokenDL();
                return getTBAccessTokenBL.GetThingsBoardAccessToken(device);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
