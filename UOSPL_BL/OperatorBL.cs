﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;
using UOSPL_DL;

namespace UOSPL_BL
{
  public  class OperatorBL
    {

        public List<uspGetOperator_Result> GetOperators(int? OperatorID)
        {
            try
            {
                OperatorDL objDL = new OperatorDL();
                return objDL.GetOperators(OperatorID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

             public List<uspGetOperatorWhenEdit_Result> GetOperatorsWhenEdit(int? OperatorID)
        {
            try
            {
                OperatorDL objDL = new OperatorDL();
                return objDL.GetOperatorsWhenEdit(OperatorID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddUpdateOperator(OperatorDetail obj)
        {
            try
            {
                OperatorDL objDL = new OperatorDL();
                return objDL.AddUpdateOperator(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
             public int DeleteOperator(int? OperatorID)
        {
            try
            {
                OperatorDL objDL = new OperatorDL();
                return objDL.DeleteOperator(OperatorID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
