﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UOSPL_Model;
using UOSPL_DL;
using System.IO;
using System.Security.Cryptography;


namespace UOSPL_BL
{
    public class AccountBL
    {
        public List<uspUserAuthentication_Result> AuthenticateUser(string userName, string password)
        {
            try
            {
                byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(password);
                byte[] passwordBytes = Encoding.UTF8.GetBytes("BikeTRacking");

                // Hash the password with SHA256
                passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

                byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

                string encryptedPassword = Convert.ToBase64String(bytesEncrypted);

                AccountDL objDL = new AccountDL();
                return objDL.AuthenticateUser(userName, encryptedPassword);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //  //authentication service for embedded side

 



        private string RandomNO()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result.ToString();
        }


        public byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            //
            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        public byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        private void Decrypt(string userName, string password)
        {
            byte[] bytesToBeDecrypted = Convert.FromBase64String(userName);
            byte[] passwordBytes = Encoding.UTF8.GetBytes("BikeTRacking");
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            string result = Encoding.UTF8.GetString(bytesDecrypted);


        }



        public int SaveUserProfile(UserProfile userProfile)
        {
            try
            {
                AccountDL objDL = new AccountDL();
                return objDL.SaveUserProfile(userProfile);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<uspGetUserProfile1_Result> GetUserProfile(string userName, string userRole)
        {
            try
            {
                AccountDL objDL = new AccountDL();
                return objDL.GetUserProfile(userName, userRole);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<uspUnusedDevices_Result> GetUnusedDevices(int? machineID)
        {
            try
            {
                AccountDL objDL = new AccountDL();
                return objDL.GetUnusedDevices(machineID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CheckMachineSrNoExist(string MachineSrNo)
        {
            try
            {
                AccountDL objDL = new AccountDL();
                return objDL.CheckMachineSrNoExist(MachineSrNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
